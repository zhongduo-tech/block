let fs = require("fs")
let path = require("path")
let __parentConfig = path.join(__dirname, "../shortcuts-manager-config.json")
global.readConfig = () => {
    let s = ""
    if (fs.existsSync(__parentConfig)){
        s = fs.readFileSync(__parentConfig, {encoding:"utf-8"})
    }else{
        return "{}"
    }
    
    
    
    return JSON.parse(s)    
}
global.saveConfig = (c)=>{
    let s = JSON.stringify(c);
    fs.writeFileSync(__parentConfig, s, {encoding:"utf-8"})
}