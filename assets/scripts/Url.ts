/**
 * resources目录下单个资源路径
 */
export const Url = {
    PREFAB: {
        TIP: "prefab/tip/Tip",
    },

    ATLAS: {
        EN: "texture/localizedImage/en/AutoAtlas",
        ZH: "texture/localizedImage/zh/AutoAtlas"
    },

    AUDIO: {
        /**全局-游戏背景Bgm */
        BGM: "audio/UI_01",
        /**底部砖块区域-点击选择后音效 */
        SFX2: "audio/UI_02",
        /**底部砖块区域-移动至棋落位后音效 */
        SFX3: "audio/UI_03",
        /**全局点击按钮背景音 */
        SFX4: "audio/UI_04",
        /**幸运奖励出现的背景音 */
        SFX5: "audio/UI_05",
        /**代币出现上浮顶部余额栏背景音 */
        SFX6: "audio/UI_06",
        /**刷新道具-触发使用背景音 */
        SFX7: "audio/UI_07",
        /**锤子道具-触发使用背景音 */
        SFX8: "audio/UI_08",
        /**抽奖-首次开始出发翻牌背景音 */
        SFX9: "audio/UI_09",
        /**抽奖-完成翻倍主动点击背景音 */
        SFX10: "audio/UI_10",
        /**失败-页面出现背景音 */
        SFX11: "audio/UI_11",
        /**全局-棋盘-逐步至灰及高亮的背景音（适用于首次默认棋盘、棋盘死亡、每次后台杀死重进） */
        SFX12: "audio/UI_12",
    }
}

/**
 * resources目录下文件夹路径
 */
export const DirUrl = {
    PREFAB: "prefab/",
    PREFAB_DIALOG: "prefab/dialog/",

    ATLAS: "atlas/",

    TEXTURE: "texture/",

    AUDIO: "audio/",
}
