import { _decorator, Component, Sprite, SpriteAtlas, SpriteFrame } from "cc";
import Res from "./Res";

const { ccclass, menu, disallowMultiple, requireComponent } = _decorator;

/**
 * 精灵组件，自动管理资源的引用计数
 */
@ccclass
@disallowMultiple
@requireComponent(Sprite)
@menu("Framework/UI组件/ResSprite")
export default class ResSprite extends Component {
    // 动态加载的资源
    private _asset: SpriteFrame | SpriteAtlas = null;

    private _sprite: Sprite = null;
    private get sprite(): Sprite {
        if (!this._sprite) {
            this._sprite = this.getComponent(Sprite);
        }
        return this._sprite;
    }

    public get spriteFrame(): SpriteFrame {
        return this.sprite.spriteFrame;
    }

    protected onDestroy(): void {
        this._asset?.decRef();
    }

    /**
     * 设置spriteFrame
     * @param url 
     * @param key 如果需要加载的url为图集时，需传入图集的key
     */
    public async setSpriteFrame(url: string, key: string = ""): Promise<void> {
        let type = key ? SpriteAtlas : SpriteFrame;
        let result = Res.get(url, type) || await Res.load(url, type);
        if (result instanceof type) {
            result.addRef();
            this._asset?.decRef();
            this._asset = result;
            this.sprite.spriteFrame = result instanceof SpriteAtlas ? result.getSpriteFrame(key) : result;
        }
    }
}
