export default {
    guide_Rtext_1: `<color = #855933><b>Drag the blocks to fill them up and they will be removed</color>`,
    guide_Rtext_2: `<color = #855933><b>You can withdraw money now, click to see</color>`,
    guide_Rtext_3: `<color = #855933><b>Select the withdrawal amount</color>`,
    guide_Rtext_4: `<color = #855933><b>Click to withdraw</color>`,
    guide_Rtext_5: `<color = #855933><b>Remove <color = #FF0606>@{1}</color> blocks\nYou can get new user rewards, up to <color = #FF0606>@{2}</color>! </color>`,
    guide_Rtext_6: `<color = #FFFFFF><b>You are receiving new user rewards\nUp to <color = #FFE766>@{1}! </color></color>`,
    guide_Rtext_7: `<color = #855933><b>Obtain <color = #FF0606>@{1}</color> again and you can withdraw <color = #FF0606>@{2}</color>! \nKeep making money! </color>`,
    home_diolog_Rtext: `<color = #000000>Obtain <color = #FF0606>@{1}</color>\nYou can withdraw <color = #FF0606>@{2}</color></color>`,
    home_withdraw_btn_text: `Withdraw`,
    home_lottery_btn_text: `lottery`,
    home_lottery_pro_Rtext: `<color = #FFFFFF><b>Eliminate <color = #FEEF33>@{1}</color> times,\nand you will win a prize</color>`,
    home_mission_btn_text: `Task reward`,
    home_curscore_text: `Score in this round`,
    home_hisscore_text: `highest in history`,
    home_todscore_text: `Today's highest`,
    pop_newcomerpage_title_text: `New user rewards`,
    pop_newcomerpage_btn_text: `confirm`,
    pop_withdrawpage_title1_text: `cash`,
    pop_withdrawpage_title2_text: `my cash`,
    pop_withdrawpage_title3_text: `Withdrawal amount`,
    pop_withdrawpage_title4_text: `selected`,
    pop_withdrawpage_cond1_Rtext: `<color = #000000><b>Insufficient cash, need to earn more <color = #FF0000>@{1}</color>. </color>`,
    pop_withdrawpage_cond2_Rtext: `<color = #000000><b>Insufficient login days, need to log in <color = #FF0000>@{1}</color> days, and eliminate <color = #FF0000>@{2}</color> times every day. </color>`,
    pop_withdrawpage_cond3_Rtext: `<color = #000000><b>Insufficient user level, need to reach <color = #FF0000>@{1}</color> level. </color>`,
    pop_withdrawpage_cond4_Rtext: `<color = #000000><b>Already reached the standard, can apply for withdrawal</color>`,
    pop_withdrawpage_btn_text: `Withdraw @{1}`,
    pop_paymentpage_title_text: `Enter information`,
    pop_paymentpage_input1_text: `Enter name`,
    pop_paymentpage_input2_text: `Enter account number (email/phone number)`,
    pop_paymentpage_btn_text: `Confirm and submit`,
    pop_confirmpage_input1_text: `Receiving account`,
    pop_confirmpage_input2_text: `Account confirmation`,
    pop_confirmpage_input3_text: `Remittance channel`,
    pop_confirmpage_btn_text: `Confirm receiving account`,
    pop_luckypage_title_text: `Lucky reward`,
    pop_luckypage_max_text: `Highest`,
    pop_luckypage_btn1_text: `Take all`,
    pop_luckypage_btn2_text: `Only get @{1}`,
    pop_gameoverpage_title1_text: `Game ends`,
    pop_gameoverpage_title2_text: `Score of this round`,
    pop_gameoverpage_word1_text: `Refresh the record!`,
    pop_gameoverpage_word2_text: `Keep up the good work!`,
    pop_gameoverpage_btn1_text: `Resurrect to receive cash`,
    pop_gameoverpage_btn2_text: `Give up resurrection and start over`,
    pop_settingpage_title_text: `Settings`,
    pop_settingpage_music_text: `Music`,
    pop_settingpage_userlevel_text: `User level`,
    pop_levelpage_title_text: `@{1} level`,
    pop_levelpage_cond_text: `The more you eliminate, the faster you upgrade`,
    pop_levelpage_btn_text: `Continue`,
    pop_levelpage_pro_text: `Upgrade progress@{1}%`,
    pop_lotterypage_title_text: `Draw`,
    pop_lotterypage_cond_text: `Select 1 card`,
    pop_lottrewardpage_title_text: `Congratulations`,
    pop_lottrewardpage_btn_text: `Receive reward`,
    pop_missionpage_title_text: `Eliminate task`,
    pop_missionpage_miscond_text: `Cumulative elimination@{1} times`,
    pop_missionpage_misbtn_text: `Receive`,
    pop_missionpage_misstate_text: `In progress`,
    pop_missionpage_misstate_text1: `Received`,
    pop_misrewardpage_title_text: `Congratulations`,
    pop_misrewardpage_btn1_text: `Double the amount`,
    pop_misrewardpage_btn2_text: `Received`,
    game_tip_text1: `Check payment information`,
    game_tip_text2: `Check name information`,
    game_tip_text3: `Check account information`,
    game_tip_text4: `Clear @{1} more times to withdraw`,
    game_tip_text5: `Insufficient balance`,
    game_tip_text6: `Insufficient cumulative login days`,
    game_tip_text7: `Insufficient level`,
    pop_settingpage_copy: `OK`,
    game_tip_noblock: `No blocks within the area`,
    pop_newlucky_title_text:`Cash Reward`
};
