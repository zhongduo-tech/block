export default {
    guide_Rtext_1: `<color = #855933><b>四角形をドラッグして塗りつぶし、削除します</color>`,
    guide_Rtext_2: `<color = #855933><b>出金が可能です。クリックして確認してください</color>`,
    guide_Rtext_3: `<color = #855933><b>出金額を選択</color>`,
    guide_Rtext_4: `<color = #855933><b>クリックして撤回</color>`,
    guide_Rtext_5: `<color = #855933><b><color = #FF0606>@{1}</color> ブロックを消去すると\n、最大 <color = #FF0606>@{2}</color> の新しいユーザー報酬を獲得できます! </color>`,
    guide_Rtext_6: `<color = #FFFFFF><b>新規ユーザー報酬を受け取ります\n最大 <color = #FFFE766>@{1}! </color></color>`,
    guide_Rtext_7: `<color = #855933><b><color = #FF0606>@{1}</color> を獲得すると、<color = #FF0606>@{2}</color> を引き出すことができます! \nお金を稼ぎ続けましょう！ </color>`,
    home_diolog_Rtext: `<color = #000000> その後、<color = #FF0606>@{1}</color> を取得すると、<color = #FF0606>@{2}</color> でお金を引き出すことができます</color>`,
    home_withdraw_btn_text: `お金を引き出す`,
    home_lottery_btn_text: `宝くじ`,
    home_lottery_pro_Rtext: `<color = #FFFFFF><b><color = #FEEF33>@{1}</color> 回倒すと、\n賞品を獲得できます</color>`,
    home_mission_btn_text: `タスク報酬`,
    home_curscore_text: `このラウンドのスコア`,
    home_hisscore_text: `史上最高額`,
    home_todscore_text: `今日の最高値`,
    pop_newcomerpage_title_text: `新規ユーザー特典`,
    pop_newcomerpage_btn_text: `確認する`,
    pop_withdrawpage_title1_text: `現金`,
    pop_withdrawpage_title2_text: `私の現金`,
    pop_withdrawpage_title3_text: `出金額`,
    pop_withdrawpage_title4_text: `選択された`,
    pop_withdrawpage_cond1_Rtext: `<color = #000000><b>現金が不足しています。<color = #FF0000>@{1}</color> をもっと稼ぐ必要があります。 </color>`,
    pop_withdrawpage_cond2_Rtext: `<color = #000000><b>ログイン日数が足りません。<color = #FF0000>@{1}</color> 日ログインして、<color = #FF0000>@{2}</color> 回クリアする必要があります毎日。 </color>`,
    pop_withdrawpage_cond3_Rtext: `<color = #000000><b>ユーザー レベルが不足しているため、<color = #FF0000>@{1}</color> レベルに達する必要があります。 </color>`,
    pop_withdrawpage_cond4_Rtext: `<color = #000000><b>基準に達したため、引き出しを申請できます</color>`,
    pop_withdrawpage_btn_text: `出金@{1}`,
    pop_paymentpage_title_text: `情報を入力してください`,
    pop_paymentpage_input1_text: `名前を入力してください`,
    pop_paymentpage_input2_text: `アカウント番号（メールアドレス/電話番号）を入力してください`,
    pop_paymentpage_btn_text: `確認して送信する`,
    pop_confirmpage_input1_text: `コレクションアカウント`,
    pop_confirmpage_input2_text: `アカウントの確認`,
    pop_confirmpage_input3_text: `支払いチャネル`,
    pop_confirmpage_btn_text: `支払いアカウントを確認する`,
    pop_luckypage_title_text: `幸運な報酬`,
    pop_luckypage_max_text: `最高`,
    pop_luckypage_btn1_text: `全部欲しい`,
    pop_luckypage_btn2_text: `@{1} のみを取得`,
    pop_gameoverpage_title1_text: `ゲームオーバー`,
    pop_gameoverpage_title2_text: `このラウンドのスコア`,
    pop_gameoverpage_word1_text: `新記録を樹立しましょう！`,
    pop_gameoverpage_word2_text: `粘り強い努力をしてください！`,
    pop_gameoverpage_btn1_text: `復活後に現金を入手`,
    pop_gameoverpage_btn2_text: `復活は諦めてやり直し`,
    pop_settingpage_title_text: `設定`,
    pop_settingpage_music_text: `音楽`,
    pop_settingpage_userlevel_text: `ユーザーレベル`,
    pop_levelpage_title_text: `@{1}レベル`,
    pop_levelpage_cond_text: `除去する数が多いほど、レベルアップが速くなります。`,
    pop_levelpage_btn_text: `続く`,
    pop_levelpage_pro_text: `アップグレードの進行状況@{1}%`,
    pop_lotterypage_title_text: `宝くじ`,
    pop_lotterypage_cond_text: `カードを1枚選択`,
    pop_lottrewardpage_title_text: `獲得おめでとうございます`,
    pop_lottrewardpage_btn_text: `報酬を請求する`,
    pop_missionpage_title_text: `タスクを削除する`,
    pop_missionpage_miscond_text: `合計 @{1} 回敗退しました`,
    pop_missionpage_misbtn_text: `受け取る`,
    pop_missionpage_misstate_text: `進行中`,
    pop_missionpage_misstate_text1: `受け取った`,
    pop_misrewardpage_title_text: `獲得おめでとうございます`,
    pop_misrewardpage_btn1_text: `2倍の量を手に入れる`,
    pop_misrewardpage_btn2_text: `受け取る`,
    game_tip_text1: `支払い情報を確認する`,
    game_tip_text2: `名前情報を確認する`,
    game_tip_text3: `アカウント情報を確認する`,
    game_tip_text4: `あと @{1} 回消去してお金を引き出す`,
    game_tip_text5: `残高不足`,
    game_tip_text6: `累計ログイン日数が足りません`,
    game_tip_text7: `レベルが足りません`,
    pop_settingpage_copy: `OK`,
    game_tip_noblock: `エリア内バリアフリー`,
    pop_newlucky_title_text:`賞金`
};
