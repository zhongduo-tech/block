import { _decorator, AudioClip, AudioSource, Component } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('AudioManager')
export class AudioManager extends Component {
	private static _ins: AudioManager;
	/** 全局唯一安全实例 */
	public static get ins(){
		return AudioManager._ins;
	}

	/** 音效类型 */
	public static get EAudio(){
		return EAudio;
	}

	@property(AudioSource)
	private bgm: AudioSource = null;
	@property(AudioSource)
	private effect: AudioSource = null;
	@property([AudioClip])
	private clip_list: AudioClip[] = [];

	private clip_map = new Map<string, AudioClip>();

	onLoad(){
		AudioManager._ins = this;
		this.init();
	}

	private init(){
		this.clip_list.forEach(clip=>this.clip_map.set(clip.name, clip));
	}

	public static playBGM(){
		if(AudioManager.ins.bgm.state == AudioSource.AudioState.PLAYING) return void 0;

		if(!AudioManager.ins.bgm.clip){
			AudioManager.ins.bgm.clip = AudioManager.ins.clip_map.get(EAudio.BGM_GLOBAL);
			AudioManager.ins.bgm.loop = true;
		}

		AudioManager.ins.bgm.play();
	}

	public static pauseBgm(){
		if(AudioManager.ins.bgm.state != AudioSource.AudioState.PLAYING) return void 0;
		AudioManager.ins.bgm.pause();
	}

	public static playEffect(effect: EAudio){
		let clip = AudioManager.ins.clip_map.get(effect);
		if(!clip) return void 0;
		AudioManager.ins.effect.playOneShot(clip);
	}

	// 静音
	public static mute(mute: boolean){
		AudioManager.ins.bgm.volume = mute ? 0 : 1;
		AudioManager.ins.effect.volume = mute ? 0 : 1;
	}
}

enum EAudio {
	/** 全局-游戏背景Bgm */
	BGM_GLOBAL = "ui_01",
	/** 游戏内-关卡挑战失败背景音 */
	BGM_DEFEAT = "ui_10",
	/** 游戏内-关卡挑战成功获胜背景音 */
	BGM_VICTORY = "ui_05",
	/** 游戏内-获得钻石奖励背景音 */
	BGM_DIAMOND = "ui_11",

	/** 全局-点击任意区域背景音 */
	EF_CLICK = "ui_02",
	/** 游戏内-消除1组卡牌背景音 */
	EF_REMOVE = "ui_03",
	/** 游戏内-获得钞票奖励上浮余额栏背景音 */
	EF_SYMBOL = "ui_04",
	/** 过度页-背景音效 */
	EF_TRANSITION = "ui_06",
	/** 游戏内-洗牌道具背景音 */
	EF_SHUFFLE = "ui_07",
	/** 游戏内-撤回/移出道具背景音 */
	EF_REVOKE = "ui_08",
	/** 游戏内-棋盘入场背景音 */
	EF_BOARD = "ui_09",
}
