import { assetManager, Game, game } from "cc";
import { parseImage } from "./parse-image";

game.on(Game.EVENT_ENGINE_INITED, onEngineInit);

function onEngineInit(){
	//enableCustomParser(); // 取消这一行的注释以启用自定义解析器
}

function enableCustomParser(){
	assetManager.parser.register({
		'.png': parseImage,
		'.jpg': parseImage,
		'.jpeg': parseImage,
	});
}