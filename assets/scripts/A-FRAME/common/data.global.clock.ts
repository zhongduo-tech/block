import { DataBlocker, DataProxy } from "../A-LIB/lib.b.data";

class Clock {
	/** 登录时间 */
	login_time: number;
	/** 当前时间 */
	current_time: number;
}

const blocker: DataBlocker<Clock> = {
	get: {
		login_time(proxy, value) {
			return value ?? Date.now();
		},
		current_time(proxy, value) {
			return Date.now();
		},
	},
}

export default function get_proxy_clock(name?: string, prefix?: string){
	return DataProxy.initProxy(name??'user', new Clock(), true, blocker, prefix??'');
}