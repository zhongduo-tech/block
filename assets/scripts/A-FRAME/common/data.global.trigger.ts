import { DataProxy } from "../A-LIB/lib.b.data";

class Trigger {
	/** 全局按钮点击事件 */
	// CLICK: {
	// 	component: string,
	// 	button: string,
	// };

	/** 游戏加载完成 */
	LOADED: boolean;
	/** 配置减灾完成 */
	CONFIG_LOADED: boolean;

	/** 新的一天 */
	// NEW_DAY: number;
}

export default function get_proxy_trigger(name?: string, prefix?: string){
	return DataProxy.initProxy(name??'trigger', new Trigger(), true, null, prefix??'');
}