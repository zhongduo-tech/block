import { native } from "cc";
import NativeClient, { NativeClientIns, NCAPIStructure } from "./NativeClient";

/** 原生端行为模拟 */
class Ba_iOSClient_aB extends NativeClientIns {
	private static receive_call_list: Array<(api: string, response: Awaited<NCAPIStructure<any, any>['response']>) => void> = [];

	public callStaticMethod(api: string, request: NCAPIStructure<any, any>['request']) {
		Ba_iOSClient_aB.request(api, request);
	}

	public onReceiveMessage(callback: (api: string, response: Awaited<NCAPIStructure<any, any>['response']>) => void) {
		Ba_iOSClient_aB.receive_call_list.push(callback);
	}

	private static request(api: string, request: NCAPIStructure<any, any>['request']) {
		native.reflection.callStaticMethod('Ba_MessageManager_aB', 'Ba_callNative_aBWithName:data:', api, JSON.stringify(request));
	}

	private static Ba_response_aB(api: string, json_string: string) {
		let response = JSON.parse(json_string) as Awaited<NCAPIStructure<any, any>['response']>;
		Ba_iOSClient_aB.receive_call_list.forEach(callback => callback(api, response));
	}
}

// NativeClient.initClient(new Ba_iOSClient_aB());
// Reflect.set(globalThis ?? window, "Ba_iOSClient_aB", Ba_iOSClient_aB);
