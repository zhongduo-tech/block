import { native } from "cc";
import NativeClient, { NativeClientIns, NCAPIStructure } from "./NativeClient";

/** Android端交互接口 */
class AndroidClient extends NativeClientIns {
	private static receive_call_list: Array<(api: string, response: Awaited<NCAPIStructure<any, any>['response']>)=>void> = [];

	public callStaticMethod(api: string, request: NCAPIStructure<any, any>['request']){
		AndroidClient.request(api, request);
	}

	public onReceiveMessage(callback: (api: string, response: Awaited<NCAPIStructure<any, any>['response']>)=>void){
		AndroidClient.receive_call_list.push(callback);
	}

	private static request(api: string, request: NCAPIStructure<any, any>['request']){
		native.reflection.callStaticMethod(
			'com/cocos/game/AndroidClient',
			'callNative',
			'(Ljava/lang/String;Ljava/lang/String;)V',
			api, JSON.stringify(request)
		);
	}

	private static response(api: string, json_string: string){
		let response = JSON.parse(json_string) as Awaited<NCAPIStructure<any, any>['response']>;
		AndroidClient.receive_call_list.forEach(callback=>callback(api, response));
	}
}

//NativeClient.initClient(new AndroidClient());
//Reflect.set(globalThis ?? window, "AndroidClient", AndroidClient);
