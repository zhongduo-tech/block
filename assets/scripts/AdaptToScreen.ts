import { _decorator, Component, Node, view, UITransform, Enum } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('AdaptToScreen')
export class AdaptToScreen extends Component {
    @property({ type: Enum({ Top: 0, Down: 1 }) })
    public position: number = 0;

    start() {
        this.adaptToScreen();
    }

    adaptToScreen() {
        const screenHeight = view.getVisibleSize().height;
        const uiTransform = this.node.getComponent(UITransform);

        if (this.position === 0) { // 适配顶部
            this.node.setPosition(this.node.position.x, screenHeight / 2 - uiTransform.height / 2);
            // console.log(mtec.cc.is_long_screen);

            if (mtec.cc.is_long_screen) {
                this.node.setPosition(this.node.position.x, this.node.position.y - 100);
            }
        } else if (this.position === 1) { // 适配底部
            this.node.setPosition(this.node.position.x, -(screenHeight / 2 - uiTransform.height / 2));
        }
    }
}
