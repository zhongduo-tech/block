import { _decorator, Button, EventTouch, Label, Node, tween, UITransform, v3 } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import GlobalData from '../../../game/GlobalData';
import { LocalStorageMgr } from '../../../game/LocalStorageMgr';
import { MainGameLogic } from '../../../game/MainGameLogic';
import { Popup } from '../page/popup';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
const { ccclass, property } = _decorator;

@ccclass('LotteryPage')
export class LotteryPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    private ui_container: UITransform;

    @property(Node)
    cardcontainer: Node = null;
    @property(Node)
    lottery: Node = null;
    @property(Node)
    lottreward: Node = null;
    @property(Node)
    candLabelNode: Node = null;

    curAwardType: number = 0;
    curAwardNum: number = 0;
    curAwardAmount: number = 0;

    @property(Node)
    shan: Node = null;

    isSeleTouch: boolean = false;

    protected pageConfig: any = null;

    public onShow(config: any) {
        AudioManager.ins.playOneShot(Url.AUDIO.SFX9, 1);
        BasePopUp.jelly_enter(this.node);
        this.pageConfig = config;
        // console.log(this.pageConfig);
        this.renderLocalUI()
    }
    protected initAfterOnLoad() {

    }

    protected _click_event_: { [name: string]: (this: LotteryPage, button: Button) => void; } = {
        getbtn(btn) {
            btn.interactable = false;
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            if (this.curAwardType == 7) {
                MainGameLogic.ins.showGetPropEffect('Hummer', v3(0, 0, 0))
                //锤子+
                GlobalData.hummerPropNum += this.curAwardNum;

                LocalStorageMgr.setItem(LocalStorageMgr.hummerPropNum_key, GlobalData.hummerPropNum);
                MainGameLogic.ins.updatePropNum();
            } else if (this.curAwardType == 8) {
                MainGameLogic.ins.showGetPropEffect('Refrash', v3(0, 0, 0))
                //刷新+
                GlobalData.refrashPropNum += this.curAwardNum;

                LocalStorageMgr.setItem(LocalStorageMgr.refrashPropNum_key, GlobalData.refrashPropNum);
                MainGameLogic.ins.updatePropNum();
            } else {
                //  钱+
                MainGameLogic.ins.updateCashLabel(this.pageConfig.cashTotal);

                MainGameLogic.ins.shwoGetCashEffect(v3(0, 0, 0));

            }
            GlobalData.curlotteryTimes++;
            LocalStorageMgr.setItem(LocalStorageMgr.curlotteryTimes_key, GlobalData.curlotteryTimes);
            GlobalData.tempElimiTimes_lottery = 0;
            LocalStorageMgr.setItem(LocalStorageMgr.tempElimiTimes_lottery_key, GlobalData.tempElimiTimes_lottery);
            MainGameLogic.ins.updateLotteryProgress();
            BasePopUp.jelly_leave(this.node).then(() => {
                btn.interactable = true;
                this.close(null);
            });
        }
    }

    start() {
        for (let i = 0; i < this.cardcontainer.children.length; i++) {
            this.cardcontainer.children[i].on(Node.EventType.TOUCH_START, this.selectionTouchCard, this);
        }
    }

    update(deltaTime: number) {

    }

    selectionTouchCard(event: EventTouch) {
        if (this.isSeleTouch) {
            return
        }
        this.isSeleTouch = true;
        AudioManager.ins.playOneShot(Url.AUDIO.SFX10, 1);
        this.candLabelNode.active = false;
        this.shuffleArray(this.pageConfig.awardsConfig);

        let card = event.getCurrentTarget() as Node;
        let isWinConfig = this.getWinningAward(this.pageConfig.awardsConfig)
        let awardType = isWinConfig.awardType;
        let awardNum = isWinConfig.awardNum;
        let awardAmount = isWinConfig.awardAmount;

        card.getChildByName('shan').active = true;
        let awardchildren = card.getChildByName('award').children
        for (let i = 0; i < awardchildren.length; i++) {
            awardchildren[i].active = false
        }
        awardchildren[awardType - 1].active = true;
        card.getChildByName('heng').active = true;
        if (awardNum > 0) {
            card.getChildByName('Label').getComponent(Label).string = "x" + awardNum.toString();
        } else if (awardAmount > 0 && awardType != 7 && awardType != 8) {
            card.getChildByName('Label').getComponent(Label).string = MainGameLogic.ins.formatCashValue(awardAmount);
        }

        tween(card)
            .to(0.15, { scale: v3(0, 1, 1) })
            .call(() => {
                card.getChildByName('card_m').active = false;
            })
            .to(0.15, { scale: v3(1, 1, 1) })
            .delay(1)
            .call(() => {
                this.otherCardShow(card.getSiblingIndex())
            })
            .delay(2)
            .call(() => {
                this.lottery.active = false
                this.showRewardPage(awardType, awardNum, awardAmount);
            })
            .start()

    }

    otherCardShow(selectedID) {
        for (let i = 0; i < this.cardcontainer.children.length; i++) {
            if (this.cardcontainer.children[i].getSiblingIndex() == selectedID) {
                continue
            }
            let otherConfig = this.pageConfig.awardsConfig[i];
            if (!otherConfig.isWinConfig) {
                let card = this.cardcontainer.children[i];
                let awardType = otherConfig.awardType;
                let awardNum = otherConfig.awardNum;
                let awardAmount = otherConfig.awardAmount;

                card.getChildByName('shan').active = false;
                let awardchildren = card.getChildByName('award').children
                for (let i = 0; i < awardchildren.length; i++) {
                    awardchildren[i].active = false
                }
                awardchildren[awardType - 1].active = true;
                card.getChildByName('heng').active = false;
                if (awardNum > 0) {
                    card.getChildByName('Label').getComponent(Label).string = "x" + awardNum.toString();
                } else if (awardAmount > 0 && awardType != 7 && awardType != 8) {
                    card.getChildByName('Label').getComponent(Label).string = MainGameLogic.ins.formatCashValue(awardAmount);
                }
                tween(card)
                    .to(0.15, { scale: v3(0, 1, 1) })
                    .call(() => {
                        card.getChildByName('card_m').active = false;
                    })
                    .to(0.15, { scale: v3(1, 1, 1) })
                    .start()
            }
        }
    }

    showRewardPage(awardType, awardNum, awardAmount) {
        this.curAwardType = awardType;
        this.curAwardNum = awardNum;
        this.curAwardAmount = awardAmount;
        this.lottreward.active = true;
        BasePopUp.jelly_enter(this.lottreward);
        tween(this.shan)
            .by(6, { angle: 360 })
            .repeatForever()
            .start()
        let awardchildren = this.lottreward.getChildByName('award').children
        if (awardType == 7) {
            for (let i = 0; i < awardchildren.length; i++) {
                awardchildren[i].active = false
            }
            awardchildren[1].active = true;
            this.lottreward.getChildByName("Label").getComponent(Label).string = "x" + awardNum.toString();
        } else if (awardType == 8) {
            for (let i = 0; i < awardchildren.length; i++) {
                awardchildren[i].active = false
            }
            awardchildren[2].active = true;
            this.lottreward.getChildByName("Label").getComponent(Label).string = "x" + awardNum.toString();
        } else {
            for (let i = 0; i < awardchildren.length; i++) {
                awardchildren[i].active = false
            }
            awardchildren[0].active = true;
            this.lottreward.getChildByName('Label').getComponent(Label).string = MainGameLogic.ins.formatCashValue(awardAmount);
        }



    }

    getWinningAward(awardsConfig: Array<{ awardType: number, awardNum: number, awardAmount: number, isWinning: boolean }>) {
        return awardsConfig.find(award => award.isWinning === true);
    }

    shuffleArray<T>(array: T[]): T[] {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array;
    }

    renderLocalUI() {
        this.isSeleTouch = false;
        this.candLabelNode.active = true;
        this.lottery.active = true;
        this.lottreward.active = false;
        for (let i = 0; i < this.cardcontainer.children.length; i++) {
            let card = this.cardcontainer.children[i];
            card.getChildByName('card_m').active = true;
            let shan = card.getChildByName('shan');
            shan.setScale(0.5, 0.5, 1);
            tween(shan)
                .by(6, { angle: 360 })
                .repeatForever()
                .start()
        }
    }

}

