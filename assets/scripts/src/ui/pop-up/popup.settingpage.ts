import { _decorator, Button, Label, ProgressBar, Node } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import type { Popup } from '../page/popup';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
import { ClientAPI } from '../../api/client.api';
import { MainGameLogic } from '../../../game/MainGameLogic';
import GlobalData from '../../../game/GlobalData';
const { ccclass, property } = _decorator;

@ccclass('SettingPage')
export class SettingPage extends BasePopUp<typeof Popup, { type: string, value: number }> {

    protected default_return: { type: string; value: number; };
    protected pageConfig: any = null;

    @property(Label)
    level: Label = null;
    @property(Label)
    id: Label = null;
    @property(ProgressBar)
    levelProgress: ProgressBar = null;

    @property(Node)
    musicOpenBtn: Node = null;
    @property(Node)
    musicCloseBtn: Node = null;

    musicIsOpen: boolean = true;

    public onShow(config: any): void {
        this.pageConfig = config;
        BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }

    protected _click_event_: { [name: string]: (this: SettingPage, button: Button) => void; } = {
        closeBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
                btn.interactable = true;
            });
        },
        copyBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            //  复制id
            ClientAPI.SetClipboard(GlobalData.deviceId);
            console.log(GlobalData.deviceId);

            MainGameLogic.ins.createOneTipTpast('pop_settingpage_copy');
        },
        levelBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            //  打开用户等级
            let level = this.pageConfig.level;
            let levelProgress = this.pageConfig.levelProgress
            this.pop.Levelpage({
                level,
                levelProgress
            })
        },
        musicBtn(btn) {
            //  音乐开关
            if (this.musicIsOpen) {
                this.musicOpenBtn.active = false;
                this.musicCloseBtn.active = true;
                this.musicIsOpen = false;
                AudioManager.ins.mute = true;
            } else {
                this.musicOpenBtn.active = true;
                this.musicCloseBtn.active = false;
                this.musicIsOpen = true;
                AudioManager.ins.mute = false;
            }
        }
    }

    start() {

    }

    update(deltaTime: number) {

    }

    async renderLocalUI() {
        this.level.string = `Lv.${this.pageConfig.level}`;

        this.id.string = `ID:${this.getFirstXCharactersInBrackets(GlobalData.deviceId, 5)}` + `....`;
        this.levelProgress.progress = this.pageConfig.levelProgress;

        if (AudioManager.ins.mute) {
            this.musicOpenBtn.active = false;
            this.musicCloseBtn.active = true;
            this.musicIsOpen = false;
        } else {
            this.musicOpenBtn.active = true;
            this.musicCloseBtn.active = false;
            this.musicIsOpen = true;
        }
    }

    /**
    * 返回字符串中 [] 内的前 x 位字符
    * @param str - 输入的字符串
    * @param x - 要返回的字符数
    * @returns 返回 [] 内的前 x 位字符，如果没有找到 []，则返回空字符串
    */
    getFirstXCharactersInBrackets(str: string, x: number): string {
        // 使用正则表达式提取 [] 内的内容
        // const match = str.match(/\[([^\]]+)\]/);
        // if (match && match[1]) {
        //     // 提取 [] 内的内容
        //     const content = match[1];
        //     // 返回前 x 位字符
        //     return content.substring(0, x);
        // }
        return str.substring(0, x);
        // return ''; // 如果没有找到 []，返回空字符串
    }


}

