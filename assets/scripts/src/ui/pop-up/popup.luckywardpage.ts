import { _decorator, Button, Component, Label, Node, Sprite, tween, UIOpacity, v3 } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import type { Popup } from '../page/popup';
import GlobalData from '../../../game/GlobalData';
import ServiceAPI from '../../api/service.api';
import { MainGameLogic } from '../../../game/MainGameLogic';
import { ClientAPI } from '../../api/client.api';
import LocalizedLabel from '../../../i18n/LocalizedLabel';
import { LocalStorageMgr } from '../../../game/LocalStorageMgr';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
import { NCPoint } from '../../api/api.config.client';
const { ccclass, property } = _decorator;

@ccclass('LuckywardPage')
export class LuckywardPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    public pageConfig: any = null;

    @property(Label)
    maxWardLabel: Label = null;
    @property(Label)
    lessWardLabel: Label = null;
    @property(Node)
    bg: Node = null;

    maxward: number = 0;
    realward: number = 0;
    lessward: number = 0;

    public onShow(config: any): void {
        this.pageConfig = config;
        // console.log("幸运弹窗奖励", config);
        AudioManager.ins.playOneShot(Url.AUDIO.SFX5, 1);

        this.maxward = this.pageConfig.cashMax;
        this.realward = this.pageConfig.cashVideoActual;
        this.lessward = this.pageConfig.cashCommonActual;

        BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }

    start() {
        this.scheduleOnce(() => {
            this.getWard();
        }, 2)
    }

    protected _click_event_: { [name: string]: (this: LuckywardPage, button: Button) => void; } = {
        async allBtn(btn) {
            ClientAPI.Point(NCPoint.REAPLUCKY, { userid: GlobalData.userId });
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            // 广告请求是否成功
            let isCom = await ClientAPI.AD("incentive",'ad');
            if (isCom) {
                btn.interactable = false;
                let config = await ServiceAPI.ForcePopReport(GlobalData.userId, this.realward, 2);
                //  加钱逻辑
                MainGameLogic.ins.updateCashLabel(config.cashTotal);
                MainGameLogic.ins.shwoGetCashEffect(v3(0, 0, 0));
                GlobalData.redPacket++;
                LocalStorageMgr.setItem(LocalStorageMgr.redPacket_key, GlobalData.redPacket);
                BasePopUp.jelly_leave(this.node).then(() => {
                    btn.interactable = true;
                    this.close(null);
                });
            }


        },
        async lessBtn(btn) {

        }
    }

    update(deltaTime: number) {

    }

    async getWard() {
        ClientAPI.Point(NCPoint.REAPLUCKY, { userid: GlobalData.userId });
        AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);

        let forceConfig = GlobalData.getDataStore().ConfCommon.forceConfig;
        let isforcePopAd = MainGameLogic.ins.getNextIntervalValue(GlobalData.luckyWardNorgetTimes, forceConfig, GlobalData.redPacket)
        if (isforcePopAd) {
            let ad_state: Parameters<typeof ClientAPI.AD>[0];
            if (GlobalData.fullscTimes < 5) {
                ad_state = "full-screen"
            } else {
                ad_state = "incentive"
            }
            // 广告请求是否成功
            let isCom = await ClientAPI.AD(ad_state,'ad');
            if (isCom) {
                GlobalData.fullscTimes++;
                LocalStorageMgr.setItem(LocalStorageMgr.fullscTimes_key, GlobalData.fullscTimes);
                let config = await ServiceAPI.ForcePopReport(GlobalData.userId, this.realward, 2);
                //  加钱逻辑
                MainGameLogic.ins.updateCashLabel(config.cashTotal);
                MainGameLogic.ins.shwoGetCashEffect(v3(0, 0, 0));
                GlobalData.redPacket++;
                LocalStorageMgr.setItem(LocalStorageMgr.redPacket_key, GlobalData.redPacket);
                GlobalData.luckyWardNorgetTimes = 0;
                LocalStorageMgr.setItem(LocalStorageMgr.luckyWardNorgetTimes_key, GlobalData.luckyWardNorgetTimes);
                await BasePopUp.jelly_leave(this.node);
            } else {
                //  少量领取上报
                let config = await ServiceAPI.ForcePopReport(GlobalData.userId, this.lessward, 3);
                // console.log("少量领取返回总钱数", config);
                //  加钱逻辑
                MainGameLogic.ins.updateCashLabel(config.cashTotal);
                MainGameLogic.ins.shwoGetCashEffect(v3(0, 0, 0));
                GlobalData.redPacket++;
                LocalStorageMgr.setItem(LocalStorageMgr.redPacket_key, GlobalData.redPacket);
                GlobalData.luckyWardNorgetTimes++;
                LocalStorageMgr.setItem(LocalStorageMgr.luckyWardNorgetTimes_key, GlobalData.luckyWardNorgetTimes);
                await BasePopUp.jelly_leave(this.node);
            }
        } else {
            //  少量领取上报
            let config = await ServiceAPI.ForcePopReport(GlobalData.userId, this.lessward, 3);
            // console.log("少量领取返回总钱数", config);
            //  加钱逻辑
            MainGameLogic.ins.updateCashLabel(config.cashTotal);
            MainGameLogic.ins.shwoGetCashEffect(v3(0, 0, 0));
            GlobalData.redPacket++;
            LocalStorageMgr.setItem(LocalStorageMgr.redPacket_key, GlobalData.redPacket);
            GlobalData.luckyWardNorgetTimes++;
            LocalStorageMgr.setItem(LocalStorageMgr.luckyWardNorgetTimes_key, GlobalData.luckyWardNorgetTimes);
            await BasePopUp.jelly_leave(this.node);
        }
        this.close(null);
    }

    renderLocalUI() {
        // this.lessWardLabel.node.active = false;
        this.maxWardLabel.string = MainGameLogic.ins.formatCashValue(this.lessward);
        // this.lessWardLabel.string = '只要' + MainGameLogic.ins.formatCashValue(this.lessward);

        // let localizedLabel = this.lessWardLabel.node.getComponent(LocalizedLabel);
        // localizedLabel.setTextKeyAndOption("pop_luckypage_btn2_text", MainGameLogic.ins.formatCashValue(this.lessward));

        // this.lessWardLabel.node.getComponent(UIOpacity).opacity = 0;
        this.lessWardLabel.node.active = true;
        // tween(this.lessWardLabel.node.getComponent(UIOpacity))
        //     .delay(2)
        //     .call(() => {
        //         this.lessWardLabel.node.active = true;
        //     })
        //     .to(0.5, { opacity: 255 })
        //     .start();
        // this.bg.setScale(1, 0, 1);
        // this.bg.getComponent(Sprite).fillRange = 0;
        // tween(this.bg)
        //     .to(0.2, { scale: v3(1, 1, 1) })
        //     .start()
        // tween(this.bg.getComponent(Sprite))
        //     .to(0.2, { fillRange: 1 })
        //     .start()

    }
}

