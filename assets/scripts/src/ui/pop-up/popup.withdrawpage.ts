import { _decorator, Button, Label, labelAssembler, log, Node, ProgressBar, RichText, UITransform } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import GlobalData from '../../../game/GlobalData';
import { Guide } from '../../../game/Guide';
import type { Popup } from '../page/popup';
import { MainGameLogic } from '../../../game/MainGameLogic';
import LocalizedLabel from '../../../i18n/LocalizedLabel';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
const { ccclass, property } = _decorator;

@ccclass('WithdrawPage')
export class WithdrawPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };

    private ui_container: UITransform;
    @property(Node)
    selecthitBtns: Node[] = [];

    @property(Label)
    wdBtnLabel: Label = null;

    /** 提现金额 */
    wdCashAmount: number = 0;
    /** 提现档位 */
    withdrawLevel: number = 0;

    @property(Label)
    cashTotalLabel: Label = null;
    @property(RichText)
    condRichText: RichText = null;
    @property(ProgressBar)
    progressbar: ProgressBar = null;
    @property(Label)
    progressLabel: Label = null;

    isCanWD: boolean = false;

    @property(Node)
    uiGuideNode2: Node = null;
    @property(Node)
    uiGuideNode3: Node = null;

    toast_game_tip_text: string = '';

    protected pageConfig: any = null;

    public onShow(config: any) {
        this.pageConfig = config;
        this.renderLocalUI();
        BasePopUp.jelly_enter(this.node);
        if (GlobalData.isCommer && GlobalData.guideRecord <= 6) {
            Guide.ins.completeCurGuide(7);
        }

        this.selecthitBtns[0].children[0].active = true;
        this.selecthitBtns[1].children[0].active = false;
        this.selecthitBtns[2].children[0].active = false;

        this.seletionBtn(0)

    }

    protected initAfterOnLoad() {
        this.ui_container = this.node.getChildByName('container').getComponent(UITransform);
    }

    protected _click_event_: { [name: string]: (this: WithdrawPage, button: Button) => void; } = {
        close(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
                btn.interactable = true;
            });
        },
        wdBtn() {
            if (this.isCanWD) {
                AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
                // if (GlobalData.isCommer) {
                //     Guide.ins.completeCurGuide(9);
                // }
                let wdcash = this.wdCashAmount;
                let wdlevel = this.withdrawLevel
                this.pop.Paymentpage({ wdcash, wdlevel })
                    .then(() => this.close(null));
            } else {
                MainGameLogic.ins.createOneTipTpast(this.toast_game_tip_text);
                if (GlobalData.isCommer) {
                    // Guide.ins.completeCurGuide(9);
                    
                    this.scheduleOnce(()=>{
                        Guide.ins.completeCurGuide(9);
                        this.close(null);
                    },1)
                    
                }
            }
        },
        noselecthit1() {
            if (GlobalData.isCommer && GlobalData.guideRecord == 7) {
                Guide.ins.completeCurGuide(8);
                return;
            }
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            if (this.selecthitBtns[0].children[0].active) {
                this.selecthitBtns[0].children[0].active = false;
                this.wdCashAmount = 0;
            } else {
                this.selecthitBtns[0].children[0].active = true;
                this.selecthitBtns[1].children[0].active = false;
                this.selecthitBtns[2].children[0].active = false;
            }
            this.seletionBtn(0)



        },
        noselecthit2() {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            if (this.selecthitBtns[1].children[0].active) {
                this.selecthitBtns[1].children[0].active = false;
                this.wdCashAmount = 0;
            } else {
                this.selecthitBtns[1].children[0].active = true;
                this.selecthitBtns[0].children[0].active = false;
                this.selecthitBtns[2].children[0].active = false;
            }
            this.seletionBtn(1)

        },
        noselecthit3() {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            if (this.selecthitBtns[2].children[0].active) {
                this.selecthitBtns[2].children[0].active = false;
                this.wdCashAmount = 0;
            } else {
                this.selecthitBtns[2].children[0].active = true;
                this.selecthitBtns[0].children[0].active = false;
                this.selecthitBtns[1].children[0].active = false;
            }
            this.seletionBtn(2)
        }
    }

    /**
     * 选择金额
     * @param btn_num 第几个按钮 0 ，1 ，2
     */
    seletionBtn(btn_num) {

        let targetCash = this.pageConfig.cashWithdrawConfig[btn_num].cashAmount;
        let activeDays = this.pageConfig.cashWithdrawConfig[btn_num].activeDays;
        let userLevel = this.pageConfig.cashWithdrawConfig[btn_num].userLevel;
        let withdrawLevel = this.pageConfig.cashWithdrawConfig[btn_num].withdrawLevel;

        let cashTotal = this.pageConfig.cashTotal;
        let loginDays = this.pageConfig.loginDays;
        let level = this.pageConfig.level;

        this.wdCashAmount = targetCash;
        this.withdrawLevel = withdrawLevel
        // this.wdBtnLabel.string = "提现" + MainGameLogic.ins.formatCashValue(this.wdCashAmount);
        let localizedLabel = this.wdBtnLabel.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("pop_withdrawpage_btn_text", MainGameLogic.ins.formatCashValue(this.wdCashAmount));

        let pro;

        if (GlobalData.curCashTotal < targetCash) {
            // this.condRichText.string = `<color = #000000><b>现金不足，需要再赚<color = #FF0000>${MainGameLogic.ins.formatCashValue(targetCash - cashTotal)}<color/><color/>`;

            let localizedLabel = this.condRichText.node.getComponent(LocalizedLabel);
            localizedLabel.setTextKeyAndOption("pop_withdrawpage_cond1_Rtext", MainGameLogic.ins.formatCashValue(targetCash - cashTotal));

            pro = this.formatFloatValue(cashTotal / targetCash);
            this.progressbar.progress = pro;
            this.progressLabel.string = `${pro * 100}%`;
            this.isCanWD = false;
            this.toast_game_tip_text = "game_tip_text5";
        } else if (loginDays < activeDays) {
            this.condRichText.string = `<color = #000000><b>登录天数不足，需要再登录<color = #FF0000>${activeDays - loginDays}<color/>天，\n并且每天消除<color = #FF0000>${100}次<color/><color/>`;

            let localizedLabel = this.condRichText.node.getComponent(LocalizedLabel);
            localizedLabel.setTextKeyAndOption("pop_withdrawpage_cond2_Rtext", activeDays - loginDays, 100);

            pro = this.formatFloatValue(loginDays / activeDays);
            this.progressbar.progress = pro;
            this.progressLabel.string = `${pro * 100}%`;
            this.isCanWD = false;
            this.toast_game_tip_text = "game_tip_text6";
        } else if (level < userLevel) {
            this.condRichText.string = `<color = #000000><b>用户等级不足，需要达到<color = #FF0000>${userLevel}级<color/><color/>`;

            let localizedLabel = this.condRichText.node.getComponent(LocalizedLabel);
            localizedLabel.setTextKeyAndOption("pop_withdrawpage_cond3_Rtext", userLevel);
            pro = this.formatFloatValue(level / userLevel);
            this.progressbar.progress = pro;
            this.progressLabel.string = `${pro * 100}%`;
            this.isCanWD = false;
            this.toast_game_tip_text = "game_tip_text7";
        } else {
            // this.condRichText.string = `<color = #000000><b>已达标，可申请提现<color/>`;
            let localizedLabel = this.condRichText.node.getComponent(LocalizedLabel);
            localizedLabel.setTextKeyAndOption("pop_withdrawpage_cond4_Rtext");

            this.progressbar.progress = 1;
            this.progressLabel.string = `${100}%`;
            this.isCanWD = true;
        }
    }


    renderLocalUI() {
        this.cashTotalLabel.string = MainGameLogic.ins.formatCashValue(this.pageConfig.cashTotal);

        let cashWithdrawConfig = this.pageConfig.cashWithdrawConfig;

        this.condRichText.string = '';
        this.selecthitBtns[0].active = false;
        this.selecthitBtns[1].active = false;
        this.selecthitBtns[2].active = false;

        if (GlobalData.isWDNewWard) {
            cashWithdrawConfig.shift();
        }
        console.log(cashWithdrawConfig);

        for (let i = 0; i < cashWithdrawConfig.length; i++) {

            this.selecthitBtns[i].active = true;
            this.selecthitBtns[i].children[1].getComponent(Label).string = MainGameLogic.ins.formatCashValue(cashWithdrawConfig[i].cashAmount);


        }
    }

    /**
    * 格式化浮点数，保留两位小数
    * 如果小数点后的值不足0.01，则返回0
    * @param value - 需要格式化的浮点数
    * @returns 格式化后的浮点数
    */
    public formatFloatValue(value: number): number {
        // 将浮点数保留两位小数
        let formattedValue = Math.floor(value * 100) / 100;

        // 如果值小于0.01，则返回0
        if (formattedValue < 0.01) {
            return 0;
        }

        // 如果格式化后的值为0，返回0.00
        return formattedValue;
    }
}
