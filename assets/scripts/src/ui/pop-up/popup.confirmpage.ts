import { _decorator, Button, Component, Label, log, Node, warnID } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import { Popup } from '../page/popup';
import GlobalData from '../../../game/GlobalData';
import { Guide } from '../../../game/Guide';
import { MainGameLogic } from '../../../game/MainGameLogic';
import ServiceAPI from '../../api/service.api';
import { Url } from '../../../Url';
import { AudioManager } from '../../../AudioManager';
import { ClientAPI } from '../../api/client.api';
import { NCPoint } from '../../api/api.config.client';
import { LocalStorageMgr } from '../../../game/LocalStorageMgr';
const { ccclass, property } = _decorator;

@ccclass('ConfirmPage')
export class ConfirmPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    protected pageConfig: any = null;

    @property(Label)
    nameLabel: Label = null;
    @property(Label)
    accountLabel: Label = null;

    @property(Label)
    cashAccountLabel: Label = null;

    public onShow(config: any): void {
        this.pageConfig = config;
        // console.log(config);
        ClientAPI.Point(NCPoint.FREESEND, { userid: GlobalData.userId });
        if (!GlobalData.isWDNewWard) {
            GlobalData.isWDNewWard = true;
            LocalStorageMgr.setItem(LocalStorageMgr.isWDNewWard_key, GlobalData.isWDNewWard);
        }


        BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }


    protected _click_event_: { [name: string]: (this: ConfirmPage, button: Button) => void; } = {
        async close(btn) {

        },
        async confirmBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
                btn.interactable = true;
                if (GlobalData.isCommer) {
                    Guide.ins.completeCurGuide(8);
                }
            });

            let config = await ServiceAPI.WithDraw(
                GlobalData.userId,
                this.pageConfig.wdlevel,
                this.pageConfig.account,
                this.pageConfig.wdcash,
                this.pageConfig.accountType,
                this.pageConfig.name,
                this.pageConfig.withdrawChannel
            );
            MainGameLogic.ins.updateCashLabel(config.cashTotal);


        }
    }

    renderLocalUI() {
        // throw new Error('Method not implemented.');
        this.nameLabel.string = this.pageConfig.name;
        this.accountLabel.string = this.pageConfig.account;
        this.cashAccountLabel.string = MainGameLogic.ins.formatCashValue(this.pageConfig.wdcash);
    }

}

