import { Prefab, resources } from 'cc';
import { ConfirmPage } from './popup.confirmpage';
import { HengfuPage } from './popup.hengfupage';
import { LuckywardPage } from './popup.luckywardpage';
import { NewcomerPage } from './popup.newcomerpage';
import { PaymentPage } from './popup.paymentpage';
import { WithdrawPage } from './popup.withdrawpage';
import { SettingPage } from './popup.settingpage';
import { LevelPage } from './popup.levelpage';
import { LotteryPage } from './popup.lotterypage';
import { MissionPage } from './popup.missionpage';
import { GameoverPage } from './popup.gameoverpage';

export enum EPopup {
    WITHDRAWPAGE,
    PAYTMENTPAGE,
    CONFIRMPAGE,
    NEWCOMERPAGE,
    HENGFUPAGE,
    LUCKYWARDPAGE,
    SETTINGPAGE,
    LEVELPAGE,
    LOTTERYPAGE,
    MISSIONPAGE,
    GAMEOVERPAGE
}

const CONFIG = {
    [EPopup.WITHDRAWPAGE]: {
        component: WithdrawPage,
        path: 'prefab/pop-up/WithdrawPage',
        script: './popup.withdrawpage.ts',
        prefab: null
    },
    [EPopup.PAYTMENTPAGE]: {
        component: PaymentPage,
        path: 'prefab/pop-up/PaymentPage',
        script: './popup.paymentpage.ts',
        prefab: null
    },
    [EPopup.CONFIRMPAGE]: {
        component: ConfirmPage,
        path: 'prefab/pop-up/ConfirmPage',
        script: './popup.confirmpage.ts',
        prefab: null
    },
    [EPopup.NEWCOMERPAGE]: {
        component: NewcomerPage,
        path: 'prefab/pop-up/NewcomerPage',
        script: './popup.newcomerpage.ts',
        prefab: null
    },
    [EPopup.HENGFUPAGE]: {
        component: HengfuPage,
        path: 'prefab/pop-up/HengfuPage',
        script: './popup.hengfupage.ts',
        prefab: null
    },
    [EPopup.LUCKYWARDPAGE]: {
        component: LuckywardPage,
        path: 'prefab/pop-up/LuckywardPage',
        script: './popup.luckywardpage.ts',
        prefab: null
    },
    [EPopup.SETTINGPAGE]: {
        component: SettingPage,
        path: 'prefab/pop-up/SettingPage',
        script: './popup.settingpage.ts',
        prefab: null
    },
    [EPopup.LEVELPAGE]: {
        component: LevelPage,
        path: 'prefab/pop-up/LevelPage',
        script: './popup.levelpage.ts',
        prefab: null
    },
    [EPopup.LOTTERYPAGE]: {
        component: LotteryPage,
        path: 'prefab/pop-up/LotteryPage',
        script: './popup.lotterypage.ts',
        prefab: null
    },
    [EPopup.MISSIONPAGE]: {
        component: MissionPage,
        path: 'prefab/pop-up/MissionPage',
        script: './popup.missionpage.ts',
        prefab: null
    },
    [EPopup.GAMEOVERPAGE]: {
        component: GameoverPage,
        path: 'prefab/pop-up/GameoverPage',
        script: './popup.gameoverpage.ts',
        prefab: null
    }
}

export async function GetPopPrefab(p: EPopup): Promise<Prefab> {
    let conf = CONFIG[p];
    if (!conf.prefab) {
        conf.prefab = await mtec.cc.loadResAsync(conf.path, Prefab, resources);
    }

    return conf.prefab;
}

export async function GetPopComponent<P extends EPopup>(p: P): Promise<(typeof CONFIG)[P]['component']> {
    let conf = CONFIG[p];
    if (!conf.component) {
        conf.component = await import(conf.script).then(m => m.default);
    }

    return conf.component;
}

export type PopCtor<P extends EPopup> = typeof CONFIG[P]['component'];

export type PopIns<P extends EPopup> = InstanceType<PopCtor<P>>;

export type PopParams<P extends EPopup> = Parameters<PopIns<P>['onShow']>;

export type PopReturn<P extends EPopup> = Awaited<ReturnType<PopIns<P>['show']>>;