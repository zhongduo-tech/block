import { _decorator, Button, Label, tween, v3 } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import { Guide } from '../../../game/Guide';
import { MainGameLogic } from '../../../game/MainGameLogic';
import { Popup } from '../page/popup';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
const { ccclass, property } = _decorator;

@ccclass('NewcomerPage')
export class NewcomerPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    protected pageConfig: any = null;

    @property(Label)
    cashLabel: Label = null;

    public onShow(config: any): void {
        this.pageConfig = config;
        // console.log(this.pageConfig.cashTotal);

        BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }


    protected _click_event_: { [name: string]: (this: NewcomerPage, button: Button) => void; } = {
        conBtn() {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            //TODO  加钱逻辑
            MainGameLogic.ins.updateCashLabel(this.pageConfig.cashTotal);
            MainGameLogic.ins.shwoGetCashEffect(v3(0, 0, 0));
            BasePopUp.jelly_leave(this.node).then(() => {
                //  引导
                Guide.ins.completeCurGuide(6)
                this.close(null);
            });
        }
    }

    renderLocalUI() {
        this.cashLabel.string = MainGameLogic.ins.formatCashValue(this.pageConfig.cashAmount);
        this.updateWardLabel(this.cashLabel, this.pageConfig.cashAmount, 0.5)
    }

    /**
     * 更新分数标签，使其逐渐增长到新分数
     * @param tarLabel 目标标签
     * @param newScore 新分数
     * @param duration 动画持续时间
     */
    updateWardLabel(tarLabel: Label, newScore: number, duration: number = 1): void {
        const currentScore = parseInt(tarLabel.string) || 0;
        const delta = newScore - currentScore;
        if (delta === 0) {
            return;
        }
        tween({ value: currentScore })
            .to(duration, { value: newScore }, {
                onUpdate: (target: { value: number }) => {
                    tarLabel.string = MainGameLogic.ins.formatCashValue(Math.floor(target.value));
                }
            })
            .start();
    }
}

