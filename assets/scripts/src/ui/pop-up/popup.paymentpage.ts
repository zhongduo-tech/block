import { _decorator, Button, EditBox } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import GlobalData from '../../../game/GlobalData';
import { Guide } from '../../../game/Guide';
import { MainGameLogic } from '../../../game/MainGameLogic';
import type { Popup } from '../page/popup';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
const { ccclass, property } = _decorator;

@ccclass('PaymentPage')
export class PaymentPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    protected pageConfig: any = null;

    @property(EditBox)
    editBoxName: EditBox = null;
    @property(EditBox)
    editBoxAccount: EditBox = null;

    public onShow(config: any): void {
        this.pageConfig = config;
        BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }


    protected _click_event_: { [name: string]: (this: PaymentPage, button: Button) => void; } = {
        close(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;

            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
                btn.interactable = true;
                // console.log("pay", GlobalData.guideRecord);

                if (GlobalData.isCommer && Guide.ins.boardeffectTimes == 1) {
                    Guide.ins.boardeffectTimes++;
                    Guide.ins.completeCurGuide(8);
                }
            });
        },

        submitBtn() {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            let wdcash = this.pageConfig.wdcash;
            let wdlevel = this.pageConfig.wdlevel
            let name = this.editBoxName.textLabel.string;
            let account = this.editBoxAccount.textLabel.string;
            let accountType = MainGameLogic.ins.determineInputType(account);
            let withdrawChannel = "Paypal"

            if (name == '' && account == '') {
                MainGameLogic.ins.createOneTipTpast('game_tip_text1');
                return
            } else if (name == '') {
                MainGameLogic.ins.createOneTipTpast('game_tip_text2');
                return
            } else if (account == '') {
                MainGameLogic.ins.createOneTipTpast('game_tip_text3');
                return
            }

            if (!accountType) {
                MainGameLogic.ins.createOneTipTpast('game_tip_text3');
                return
            }
            this.pop.Confirmpage({
                wdcash, wdlevel, name, account, accountType, withdrawChannel
            }).then((res) => this.close(null));
        }
    }

    renderLocalUI() {
        // throw new Error('Method not implemented.');
    }
}

