import { _decorator, Button, Label, tween } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import GlobalData from '../../../game/GlobalData';
import { GridBlockMgr } from '../../../game/GridBlockMgr';
import LocalizedLabel from '../../../i18n/LocalizedLabel';
import { ClientAPI } from '../../api/client.api';
import { Popup } from '../page/popup';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
import { GridBoardData } from '../../../game/Block/BlockData';
import { MainGameLogic } from '../../../game/MainGameLogic';
const { ccclass, property } = _decorator;

@ccclass('GameoverPage')
export class GameoverPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };

    @property(Label)
    scoreLabel: Label = null;
    @property(Label)
    condLabel: Label = null;

    public onShow(): void {
        BasePopUp.jelly_enter(this.node);
        AudioManager.ins.playOneShot(Url.AUDIO.SFX11, 1);
        this.renderLocalUI();
    }

    protected _click_event_: { [name: string]: (this: GameoverPage, button: Button) => void; } = {
        async resurreBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            let isCom = await ClientAPI.AD("incentive",'ad');
            if (isCom) {
                MainGameLogic.ins.useHummerProp(true);
                MainGameLogic.ins.useRefrashProp(true);
            }
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
            });
        },
        giveupBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            //  重新开始
            MainGameLogic.ins.reStartGame()
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
            });
        }

    };


    renderLocalUI() {
        let localizedLabel = this.condLabel.node.getComponent(LocalizedLabel);
        this.scoreLabel.string = GlobalData.curScore.toString();
        // this.updateWardLabel(this.scoreLabel, GlobalData.curScore, 0.5)
        if (GlobalData.curScore >= GlobalData.historyHighScore) {
            localizedLabel.setTextKeyAndOption("pop_gameoverpage_word1_text");
        } else {
            localizedLabel.setTextKeyAndOption("pop_gameoverpage_word2_text");
        }
    }

    /**
     * 更新分数标签，使其逐渐增长到新分数
     * @param tarLabel 目标标签
     * @param newScore 新分数
     * @param duration 动画持续时间
     */
    updateWardLabel(tarLabel: Label, newScore: number, duration: number = 1): void {
        const currentScore = parseInt(tarLabel.string) || 0;
        const delta = newScore - currentScore;
        if (delta === 0) {
            return;
        }
        tween({ value: currentScore })
            .to(duration, { value: newScore }, {
                onUpdate: (target: { value: number }) => {
                    tarLabel.string = target.value.toString();
                }
            })
            .start();
    }


}

