import { _decorator, Button, Component, Label, labelAssembler, Node, ProgressBar } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import { Popup } from '../page/popup';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
import LocalizedLabel from '../../../i18n/LocalizedLabel';
const { ccclass, property } = _decorator;

@ccclass('LevelPage')
export class LevelPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    protected pageConfig: any = null;

    @property(Label)
    titleLabel: Label = null;
    @property(ProgressBar)
    levelProgress: ProgressBar = null;
    @property(Label)
    levelProgressLabel: Label = null;

    public onShow(config: any): void {
        this.pageConfig = config;
        BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }

    protected _click_event_: { [name: string]: (this: LevelPage, button: Button) => void; } = {
        closeBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
                btn.interactable = true;
            });
        },
        conBtn(btn) {
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;
            BasePopUp.jelly_leave(this.node).then(() => {
                this.close(null);
                btn.interactable = true;
            });
        },

    }

    start() {

    }

    update(deltaTime: number) {

    }
    renderLocalUI() {
        // this.titleLabel.string = `${this.pageConfig.level}级`;
        let localizedLabel = this.titleLabel.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("pop_levelpage_title_text", this.pageConfig.level);
        this.levelProgress.progress = this.pageConfig.levelProgress;
        // this.levelProgressLabel.string = `${this.pageConfig.levelProgress * 100}%`;

        let localizedLabel1 = this.levelProgressLabel.node.getComponent(LocalizedLabel);
        localizedLabel1.setTextKeyAndOption("pop_levelpage_pro_text", (this.pageConfig.levelProgress * 100));

    }
}

