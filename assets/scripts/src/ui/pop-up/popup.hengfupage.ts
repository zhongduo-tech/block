import { _decorator, easing, RichText, tween, v3 } from 'cc';
import { BasePopUp } from '../../../A-FRAME/component/ui.pop-up';
import { MainGameLogic } from '../../../game/MainGameLogic';
import LocalizedLabel from '../../../i18n/LocalizedLabel';
import type { Popup } from '../page/popup';
const { ccclass, property } = _decorator;

@ccclass('HengfuPage')
export class HengfuPage extends BasePopUp<typeof Popup, { type: string, value: number }> {
    protected default_return: { type: string; value: number; };
    protected pageConfig: any = null;

    @property(RichText)
    label: RichText = null;

    public onShow(config: any): void {
        this.pageConfig = config;
        // BasePopUp.jelly_enter(this.node);
        this.renderLocalUI();
    }
    async start() {
        this.scheduleOnce(() => {
            this.pop.Newcomerpage(this.pageConfig);
        }, 2)
        this.scheduleOnce(() => {
            this.close(null)
        }, 3)
    }

    update(deltaTime: number) {

    }
    renderLocalUI() {
        this.node.setScale(0, 0, 1);
        tween(this.node)
            .to(0.1, { scale: v3(1, 1, 1) }, { easing: "backOut" })
            .start();
        // this.label.string = `<color=#FFFFFF><b>正在抽取新人福利，最高<color=#FFE766>${MainGameLogic.ins.formatCashValue(this.pageConfig.showAmount)}</color>`;
        let localizedLabel = this.label.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("guide_Rtext_6", MainGameLogic.ins.formatCashValue(this.pageConfig.showAmount));

    }
}

