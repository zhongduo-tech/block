import { _decorator, Button } from 'cc';
import { BaseUI } from '../../../A-FRAME/component/ui.base';
import { AudioManager } from '../../../AudioManager';
import { Url } from '../../../Url';
import GlobalData from '../../../game/GlobalData';
import { LocalStorageMgr } from '../../../game/LocalStorageMgr';
import { MainGameLogic } from '../../../game/MainGameLogic';
import ServiceAPI from '../../api/service.api';
import { Popup } from './popup';
const { ccclass, property } = _decorator;

@ccclass('PageHome')
export class PageHome extends BaseUI {

    protected initAfterOnLoad(): void {

    }

    protected start() {

    }

    private a: string;

    protected _click_event_: { [name: string]: (this: PageHome, button: Button) => void; } = {
        async cashbtn(btn) {
            if(Popup.lock){
                return void 0;
            }
            Popup.lock = true;

            if (GlobalData.isCommer && GlobalData.guideRecord <= 3) {
                return
            }
            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            let config =  await ServiceAPI.WithdrawConfig(GlobalData.userId);
            let wdlevelcashdata = config.cashWithdrawConfig;
            if (wdlevelcashdata.length == 2) {
                GlobalData.wdlevelCahs = wdlevelcashdata[0].cashAmount;
                LocalStorageMgr.setItem(LocalStorageMgr.wdlevelCahs_key, GlobalData.wdlevelCahs);
                
            }
            await Popup.Withdrawpage(config);
            // await Popup.Paymentpage({ wdcash: 100, wdlevel: 100 })

            Popup.lock = false;
        },
        async SettingButton(btn) {
            if(Popup.lock){
                return void 0;
            }
            Popup.lock = true;

            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            let config = await ServiceAPI.userInfo(GlobalData.userId);
            // let level = GlobalData.getDataStore().userInfo.level;
            // let levelProgress = GlobalData.getDataStore().userInfo.levelProgress;
            // let id = GlobalData.userId;
            await Popup.Settingpage(config);

            Popup.lock = false;
        },
        async RewardButton(btn) {
            if(Popup.lock){
                return void 0;
            }
            Popup.lock = true;

            AudioManager.ins.playOneShot(Url.AUDIO.SFX4, 1);
            btn.interactable = false;
            // let config = await ServiceAPI.ConfCommon(GlobalData.userId);
            // console.log(GlobalData.missionData);

            await Popup.Missionpage(GlobalData.missionData);
            Popup.lock = false;

            btn.interactable = true;
        },
        icon_lottery(btn) {
            let lotteryData = GlobalData.getDataStore().ConfCommon.lotteryConfig;

            try {
                const curLotteryTimes = GlobalData.curlotteryTimes;

                for (let rule of lotteryData) {
                    if (curLotteryTimes >= rule.minimum && curLotteryTimes <= rule.maximum) {

                        MainGameLogic.ins.createOneTipTpast('game_tip_text4', (rule.needElimTimes - GlobalData.tempElimiTimes_lottery).toString())
                        // console.log("吐司");
                        // let localizedLabel = this.lottoryLabel.node.getComponent(LocalizedLabel);
                        // localizedLabel.setTextKeyAndOption("home_lottery_pro_Rtext", rule.needElimTimes - GlobalData.tempElimiTimes_lottery);

                        // if (GlobalData.tempElimiTimes_lottery == rule.needElimTimes) {
                        //     try {
                        //         let config = await ServiceAPI.LotteryConfig(GlobalData.userId);
                        //         Popup.Lotterypage(config);
                        //     } catch (error) {
                        //         console.error('Error fetching lottery config:', error);
                        //     }
                        // }
                        // break;
                    }
                }
            } catch (error) {
                console.error('Error in updateLotteryProgress:', error);
            }

        }

    }


}

