export const DEFAULT_BODY: { version: string; product: string; platform: 'ios' | 'android' } = {
	version: "10805007",
	product: "block",
	platform: "android",
}

export enum HSAPIURI {
	UserInit = "block/user/init",
	UserInfo = "block/user/info",
	ConfCountry = "block/config/country",
	ConfCommon = "block/config/common",
	WithdrawConfig = "block/config/withdrawConfig",
	GetNewComerReward = "block/reward/v2/getFresh",
	LotteryConfig = "block/reward/lotteryConfig",
	ForcePopConfig = "block/reward/v2/forcePopConfig",
	ForcePopReport = "block/reward/forcePopReport",
	MissionReport = "block/user/missionCashReport",
	EliminateReport = "block/user/v2/eliminateReport",
	WithDraw = "block/symbol/withdraw",
}

export interface HSAPIStructMap {
	[HSAPIURI.UserInit]: api_structure_template<{
		/** 初始化用户的设备ID */
		deviceId: string;
		/**
		 * 国家代码
		 * {@link https://q178jmgy9tl.feishu.cn/sheets/MaWdsru9ihFVVZtb7LCcGjy6nef}
		 */
		country: string;
	}, {
		/** 用户ID */
		userId: number;
		/** 用户的国家配置 */
		country: string;
		/** 用户的语言配置 */
		language: string;
	}>,
	[HSAPIURI.UserInfo]: api_structure_template<{
		/** 初始化用户的设备ID */
		userId: number;
	}, {
		/** 用户国家信息 */
		country: string;
		/** 用户语言信息 */
		language: string;
		/** 账户现金总金额 */
		cashTotal: number;
		/** 用户等级 */
		level: number;
		/** 升级进度 （小数）*/
		levelProgress: number;
		/** 红包数 */
		redPacket: number;
	}>,
	[HSAPIURI.ConfCountry]: api_structure_template<{
		/** 国家代码 */
		country: string;
	}, {
		/**
		 * 国家代码
		 * {@link https://q178jmgy9tl.feishu.cn/sheets/MaWdsru9ihFVVZtb7LCcGjy6nef}
		 */
		country: string;
		/** 语言代码 */
		language: string;
		/** 货币符号 */
		symbol: string;
		/**
		 * 货币格式是否需要去整
		 * [1]: 需要;
		 * [0]: 不需要;
		 */
		round: 1 | 0;
		/**
		 * 货币格式符号前置
		 * [1]: 前置;
		 * [0]: 后置;
		 */
		front: 1 | 0;
		/**
		 * 千位符
		 * [1]: ".";
		 * [2]: ",";
		 * [3]: " ";
		 */
		thousand: 1 | 2 | 3;
		/**
		 * 基数点
		 * [1]: ".";
		 * [2]: ",";
		 * [3]: " ";
		 */
		point: 1 | 2 | 3;
		/** 常用支付渠道 */
		channel: string[];
	}>,
	[HSAPIURI.ConfCommon]: api_structure_template<{
		/** 用户ID */
		userId: number;
	}, {
		/** 红包弹板消除次数配置 */
		popEliminateCount: number;
		/** 累计消除总数 */
		eliminateTotal: number;
		/** 累计抽奖次数 */
		drawTotal: number;
		/** 强弹配置 */
		forceConfig: Array<{
			/** 红包下限 */
			redStartCount: number;
			/** 红包上限 */
			redEndCount: number;
			/** ：是否达成任务条件 */
			forcePopIntervalList: Array<number>;
		}>;
		/** 任务列表配置 */
		missionConfig: Array<{
			/** 消除次数 */
			eliminateCount: number;
			/** 任务等级 */
			missionLevel: number;
			/** 任务id */
			taskId: number;
			/** ：是否达成任务条件 */
			isComplete: boolean;
			/** ：是否领取奖励 */
			isGetReward: boolean;
			/** 任务完成后的现金奖励 */
			cashReward: number;
		}>;
		/** 抽奖次数配置 */
		lotteryConfig: Array<{
			/** 次数上限 */
			maximum: number;
			/** 次数下限 */
			minimum: number;
			/** 所需消除次数 */
			needElimTimes: number
		}>;
	}>,
	[HSAPIURI.WithdrawConfig]: api_structure_template<{
		/** 用户ID */
		userId: number;
	}, {
		/** 账户现金总金额 */
		cashTotal: number;
		/** 累计登录天数 */
		loginDays: number;
		/** 用户等级 */
		level: number;
		/** 现金提款档位配置 */
		cashWithdrawConfig: Array<{
			/** 提现金额 */
			cashAmount: number;
			/** 活跃天数 */
			activeDays: number;
			/** 用户等级 */
			userLevel: number;
		}>;
	}>,
	[HSAPIURI.GetNewComerReward]: api_structure_template<{
		/** 用户ID */
		userId: number;
	}, {
		/** 奖励展示的现金金额 */
		showAmount: number;
		/** 奖励的现金金额 */
		cashAmount: number;
		/** 现金总额 */
		cashTotal: number;
	}>,
	[HSAPIURI.LotteryConfig]: api_structure_template<{
		/** 用户ID */
		userId: number;
	}, {
		/** 奖励类型配置 */
		awardsConfig: Array<{
			/** 类型-序号 */
			awardType: number;
			/** 奖励数量-道具 */
			awardNum: number;
			/** 奖励数量-现金 */
			awardAmount: number;
			/** 是否是中奖结果 */
			isWinning: boolean
		}>;
		/** 现金总数 */
		cashTotal: number
	}>,
	[HSAPIURI.ForcePopConfig]: api_structure_template<{
		/** 用户ID */
		userId: number;
	}, {
		/** 最高金额 */
		cashMax: number;
		/** 广告领取金额 */
		cashVideoActual: number;
		/** 少量领取金额 */
		cashCommonActual: number;
	}>,
	[HSAPIURI.ForcePopReport]: api_structure_template<{
		/** 用户ID */
		userId: number;
		/** 获得现金额度*/
		packCash: number;
		/** 领取方式 2 视频领取 3 普通领取*/
		packGetType: number;
	}, {
		/** 更新后的现金总额 */
		cashTotal: number;
	}>,
	[HSAPIURI.MissionReport]: api_structure_template<{
		/** 用户ID */
		userId: number;
		/** 任务id*/
		taskId: number;
		/** 是否双倍 */
		isTaskGetDouble: boolean;
	}, {
		/** 更新后的现金总额 */
		cashTotal: number;
	}>,
	[HSAPIURI.EliminateReport]: api_structure_template<{
		/** 用户ID */
		userId: number;
		/** 是否是新人引导 */
		isFreshGuide: boolean;
	}, {
		/** 红包数 */
		packageCount: number;
		/** 小额现金额度 */
		cashAmount: number;
		/** 现金总额 */
		cashTotal: number;
	}>,
	[HSAPIURI.WithDraw]: api_structure_template<{
		/** 用户ID */
		userId: number;
		/** 提现档位 */
		withdrawLevel: number;
		/** 打款账户 */
		account: string;
		/** 提现金额 */
		withdrawCash: number;
		/** 账户类型  EMAIL PHONE*/
		accountType: string;
		/** 联系人姓名 */
		concatName: string;
		/** 提现渠道 PayPal */
		withdrawChannel: string;
	}, {
		/** 提现后用户总金额 */
		cashTotal: number;
	}>,
}

export type HSReq<A extends HSAPIURI> = HSAPIStructMap[A]['request'];
export type HSRes<A extends HSAPIURI> = HSAPIStructMap[A]['response'];
export type HSBody<A extends HSAPIURI> = HSReq<A>['data'];
export type HSResult<A extends HSAPIURI> = Awaited<HSRes<A>>['result'];

type request_template<body> = {
	/** cocos 请求唯一标识 */
	ccToken: string;
	/** app版本号 */
	version: string;
	/** 产品名称 */
	product: string;
	/** 平台 ios android */
	platform: string;
	/** 接口请求参数 */
	data: body;
}

type response_template<result> = {
	/** cocos 请求唯一标识 */
	ccToken: string;
	/** 响应是否成功 */
	success: boolean;
	/** 响应信息 */
	message: string;
	/** 响应码 */
	code: string;
	/** 响应数据 */
	result: result;
}

/** API结构体 */
type api_structure_template<body, result> = {
	/** 接口标识符 */
	api: HSAPIURI;
	/** 请求数据 */
	request: request_template<body>;
	/** 响应数据 */
	response: Promise<response_template<result>>
}
