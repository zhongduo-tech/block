import { NCAPIStructure } from "../../A-FRAME/native/NativeClient";

export type NCRequest<uri extends NCAPIURI> = NCAPIStructMap[uri]['request'];
export type NCResponse<uri extends NCAPIURI> = NCAPIStructMap[uri]['response'];
export type NCBody<uri extends NCAPIURI> = NCRequest<uri>['data'];
export type NCResult<uri extends NCAPIURI> = Awaited<NCResponse<uri>>['data'];

export enum NCAPIURI {
	/** 设置剪贴板 */
	SetClipboard = 'set_clipboard',
	/** 广告接口 */
	AD = 'AD',
	/** 本地化接口 */
	LOCAL = 'LocalLanguage',
	/** 外部链接展示接口 */
	LINK = 'Link',
	/** 浮层弹窗 */
	Float = 'float-pop',
	/** 设备ID */
	DeviceID = 'device_id',
	/** 评分弹窗 */
	ScorePop = 'ScorePop',
	/** 关闭加载页 */
	CloseLoading = 'closeLoading',
	/** 关卡上报 */
	levelResult = 'levelResult',
	/** 埋点上报接口 */
	Point = 'POINT'
}

interface NCAPIStructMap {
	[NCAPIURI.SetClipboard]: NCAPIStructure<{
		/** 要设置的文本 */
		text: string;
	}, {}>;
	[NCAPIURI.AD]: NCAPIStructure<{
		/**
		 * 广告类型\
		 *  [incentive]: 激励广告\
		 *  [full-screen]: 全屏广告
		 */
		type: 'incentive' | 'full-screen',
		floatType: 'ad' | 'prop'
	}, {
		/**
		 * 广告结束状态\
		 *  [ad-over]: 广告播放结束\
		 *  [ad-error]: 广告播放失败
		 */
		status: `ad-${'over' | 'error'}`
	}>;
	[NCAPIURI.DeviceID]: NCAPIStructure<{}, {
		/** 用户设备ID */
		device_id: string;
	}>;
	[NCAPIURI.LOCAL]: NCAPIStructure<{}, {
		/**
		 * 国家码
		 *  https://q178jmgy9tl.feishu.cn/sheets/MaWdsru9ihFVVZtb7LCcGjy6nef
		 */
		countryCode: string;
		/** 语言码 */
		languageCode: string;
	}>;
	[NCAPIURI.LINK]: NCAPIStructure<{
		/** 要显示的链接 */
		url: string;
	}, {}>;
	[NCAPIURI.Float]: NCAPIStructure<{
		/** 展示弹窗类型 */
		type: 'ad' | 'prop';
	}, {}>;
	[NCAPIURI.ScorePop]: NCAPIStructure<{}, {}>;
	[NCAPIURI.CloseLoading]: NCAPIStructure<{}, {}>;
	[NCAPIURI.levelResult]: NCAPIStructure<{
		/**
		 * 关卡数
		 */
		level: string;
		/**
		* 闯关结果
		* 0： 失败
		* 1： 成功
		*/
		result: "1" | "0";
	}, {}>;
	[NCAPIURI.Point]: NCAPIStructure<{
		/** 点位标识符 */
		point: string;
		/** 点位数据，格式化的字符串 */
		value: string;
	}, {}>;
}

export type NCPointData<point extends NCPoint> = NCPointStructMap[point];

/** 原生埋点标识符 */
export enum NCPoint {
	/** 发起打款用户数 */
	FREESEND = 'freeSend',
	/** 幸运奖励红包数领取数 */
	REAPLUCKY = 'reapLucky',
	/** 任务达成数 */
	TASKCOMPLETE = 'taskComplete',
	/** 任务领取数 */
	TASKDONE = 'taskDone',
	/** 余额满足当前档位用户数 */
	SYMBOLENOUGH = 'symbolEnough',
	/** cocos开始加载资源，整个app生命周期上报一次 */
	GAMEINITSTART = 'gameInitStart',
	/** cocos加载资源完成，整个app生命周期上报一次 */
	GAMEINITFINISH = 'gameInitFinish',
	/** 进入游戏，整个app生命周期上报一次 */
	GAMESTART = 'gameStart'
}

/** 原生埋点接口上报结构 */
interface NCPointStructMap {
	/** 发起打款用户数 */
	[NCPoint.FREESEND]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** 幸运奖励红包数领取数 */
	[NCPoint.REAPLUCKY]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** 任务达成数 */
	[NCPoint.TASKCOMPLETE]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** 任务领取数 */
	[NCPoint.TASKDONE]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** 余额满足当前档位用户数 */
	[NCPoint.SYMBOLENOUGH]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** cocos开始加载资源，整个app生命周期上报一次 */
	[NCPoint.GAMEINITSTART]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** cocos加载资源完成，整个app生命周期上报一次 */
	[NCPoint.GAMEINITFINISH]: {
		/** 用户在服务器的ID */
		userid: number;
	},
	/** 进入游戏，整个app生命周期上报一次 */
	[NCPoint.GAMESTART]: {
		/** 用户在服务器的ID */
		userid: number;
	}
}