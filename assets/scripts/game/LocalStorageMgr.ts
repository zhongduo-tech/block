import { _decorator } from 'cc';
import { APP_NAME } from '../A-FRAME/frame.config';
const { ccclass, property } = _decorator;

@ccclass('LocalStorageMgr')
export class LocalStorageMgr {
    public static userId_key = 'userId_key';
    public static country_key = 'country_key';
    public static language_key = 'language_key';
    public static device_id_key = 'device_id_key';
    public static localdataStore = {
        initUser: 'initUser',
        userInfo: 'userInfo',
        ConfCountry: 'ConfCountry',
        ConfCommon: 'ConfCommon',
        WithdrawConfig: 'WithdrawConfig',
        GetNewComerReward: 'GetNewComerReward',
        LotteryConfig: 'LotteryConfig',
        ForcePopConfig: 'ForcePopConfig',
        ForcePopReport: 'ForcePopReport',
        MissionReport: 'MissionReport',
        EliminateReport: 'EliminateReport',
        WithDraw: 'WithDraw',
    };
    public static isWDNewWard_key = 'isWDNewWard_key';
    public static gameInitFinish_key = "gameInitFinish_key";
    public static fullscTimes_key = 'fullscTimes_key';
    public static refrashPropNum_key = 'refrashProp_key';
    public static hummerPropNum_key = 'hummerProp_key';
    public static luckyWardNorgetTimes_key = 'luckyWardNorgetTimes_key';
    public static curlotteryTimes_key = 'curlotteryTimes_key';
    public static isCommer_key = 'isCommer_key';
    public static guideRecord_key = 'guideRecord_key';
    public static localBoard_key = 'localBoard_key';
    public static elimiTimes_key = 'elimiTimes_key';
    public static tempElimiTimes_lucky_key = 'tempElimiTimes_lucky_key';
    public static tempElimiTimes_lottery_key = 'tempElimiTimes_lottery_key';
    public static lastGameBoardData_key = 'lastGameBoardData_key';
    public static lastLoginDate_key = 'lastLoginDate_key';
    public static historyHighScore_key = 'historyHighScore_key';
    public static todayHighScore_key = 'todayHighScore_key';
    public static curScore_key = 'curScore_key';
    static wdlevelCahs_key: string = 'wdlevelCahs_key';
    static redPacket_key: string = 'redPacket_key';

    /**
    * 存储数据到本地存储中
    * @param key APP_NAME + 存储数据的键
    * @param value 存储的数据，任意类型
    */
    public static setItem(key: string, value: any): void {
        localStorage.setItem(APP_NAME + key, JSON.stringify(value));
    }

    /**
    * 从本地存储中获取数据
    * @param key APP_NAME + 存储数据的键
    * @param defaultValue 如果没有找到指定键的数据，返回的默认值
    * @returns 返回找到的数据或默认值
    */
    public static getItem<T>(key: string): T {
        const value = localStorage.getItem(APP_NAME + key);

        if (value === null || value === undefined) {
            // 如果值为 null 或 undefined，则保存之后返回默认值
            return null;
        }
        try {
            return JSON.parse(value) as T;
        } catch (e) {
            console.error(`Failed to parse local storage data: ${APP_NAME + key}`, e);
            return null;
        }
    }

    /**
    * 判断本地存储中是否有指定键的数据
    * @param key APP_NAME + 存储数据的键
    * @returns 返回是否存在指定键的数据
    */
    public static hasItem(key: string): boolean {
        const value = localStorage.getItem(APP_NAME + key);
        return value !== null && value !== undefined;
    }
    /**
    * 移除本地存储中指定键的数据
    * @param key APP_NAME + 存储数据的键
    */
    public static removeItem(key: string): void {
        localStorage.removeItem(APP_NAME + key);
    }
    /**
    * 清空本地存储中所有数据
    */
    public static clear(): void {
        localStorage.clear();
    }
}

