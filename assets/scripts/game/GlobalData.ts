import { LocalStorageMgr } from "./LocalStorageMgr";

interface DataStore {
    initUser,
    userInfo,
    ConfCountry,
    ConfCommon,
    WithdrawConfig,
    GetNewComerReward,
    LotteryConfig,
    ForcePopConfig,
    ForcePopReport,
    MissionReport,
    EliminateReport,
    WithDraw,
}


export default class GlobalData {


    //  ⬇️服务器获取-存本地
    /** 用户id */
    public static userId: number;
    /** 设备id */
    public static deviceId: string;
    /** 国家代码 */
    public static country: string;
    /** 语言信息 */
    public static language: string;
    /** 幸运奖励弹窗间隔 */
    public static luckyWardPageInterval: number;
    public static luckyWardNorgetTimes: number = 0;
    /** 当前钱数 */
    public static curCashTotal: number;
    /** 领取红包数 */
    public static redPacket: number;
    /** 任务配置 */
    public static missionData: any;


    //  ⬇️纯本地存储
    /** 强弹全屏次数*/
    public static fullscTimes: number = 0;
    /** 是否新手 */
    public static isCommer: boolean = true;
    /** 引导记录 */
    public static guideRecord: number = 1;
    /** 消除累计次数 */
    public static elimiTimes: number = 0;
    /** 临时消除累计次数-判断幸运弹窗 */
    public static tempElimiTimes_lucky: number = 0;
    /** 实时保存的棋盘数据 */
    public static lastGameBoard: number[][] = [
        [1, 1, 0, 0, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 0, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 1, 1]
    ];

    public static refrashPropNum: number = 3;
    public static hummerPropNum: number = 3;

    /** 上次的登录日期 */
    public static lastLoginDate: string = 'Tue Aug 20 2023';
    /** 历史最高分 */
    public static historyHighScore: number = 0;
    /** 今日最高分 */
    public static todayHighScore: number = 0;
    /** 当前分数 */
    public static curScore: number = 0;
    /** 分数上限 */
    public static addScoreUp: number = 10;
    /** 分数下限 */
    public static addScoreFloor: number = 5;

    /** 当前抽奖次数 */
    public static curlotteryTimes: number = 0;
    /** 临时消除累计次数-判断抽奖 */
    public static tempElimiTimes_lottery: number = 0;

    // 通用的数据存储对象
    private static dataStore: DataStore = {
        initUser: undefined,
        userInfo: undefined,
        ConfCountry: undefined,
        ConfCommon: undefined,
        WithdrawConfig: undefined,
        GetNewComerReward: undefined,
        LotteryConfig: undefined,
        ForcePopConfig: undefined,
        ForcePopReport: undefined,
        MissionReport: undefined,
        EliminateReport: undefined,
        WithDraw: undefined
    };
    static wdlevelCahs: number;
    static isWDNewWard: boolean = false;

    // 通用方法用于更新任意数据
    public static async fetchAndUpdateData(property: keyof DataStore, fetchMethod: () => Promise<any>): Promise<void> {
        try {
            const result = await fetchMethod();
            if (result) {
                this.dataStore[property] = result;
                LocalStorageMgr.setItem(LocalStorageMgr.localdataStore[property], this.dataStore)
                // console.log(`${property} updated:`, result);
            } else {
                console.error(`Failed to fetch ${property}`);
            }
        } catch (error) {
            console.error(`Error fetching ${property}:`, error);
        }
    }

    // 获取数据存储对象
    public static getDataStore(): DataStore {
        return this.dataStore;
    }

}


