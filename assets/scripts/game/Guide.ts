import { _decorator, Component, find, Mask, Node, RichText, Size, Tween, tween, UIOpacity, UITransform, v3, Vec3 } from 'cc';
import LocalizedLabel from '../i18n/LocalizedLabel';
import ServiceAPI from '../src/api/service.api';
import { Popup } from '../src/ui/page/popup';
import { BlockController } from './Block/BlockController';
import { BlocksAll, GridBoardData } from './Block/BlockData';
import GlobalData from './GlobalData';
import { GridBlockMgr } from './GridBlockMgr';
import { LocalStorageMgr } from './LocalStorageMgr';
import { MainGameLogic } from './MainGameLogic';
import { TouchMgr } from './TouchMgr';
import { WithdrawPage } from '../src/ui/pop-up/popup.withdrawpage';
const { ccclass, property } = _decorator;

@ccclass('Guide')
export class Guide extends Component {
    /** 单例模式 */
    private static _ins: Guide;
    constructor() {
        super();
        Guide._ins = this;
    }
    public static get ins(): Guide {
        if (!Guide._ins) {
            Guide._ins = new Guide();
        }
        return Guide._ins;
    }

    newComerReward: any = null;

    @property(Mask)
    private mask: Mask = null;
    @property(Node)
    shade: Node = null;
    @property(UITransform)
    private toucher: UITransform;
    @property(UITransform)
    private pointer: UITransform = null;

    @property(Node)
    block: Node = null;
    @property(Node)
    diolog: Node = null;
    @property(RichText)
    diologLabel: RichText = null

    guideindex1: number[] = [3, 3];
    guideindex2: number[] = [3, 3];
    guideindex3: number[] = [4, 3];

    @property(Node)
    guidenode: Node = null;
    guideTarPos: Vec3 = null;
    @property(Node)
    selecnode: Node = null;
    selecOriPos: Vec3 = null;
    @property(Node)
    uiGuideNode1: Node = null;
    uiGuideNodePos1: Vec3 = null;
    @property(Node)
    uiGuideNode2: Node = null;
    uiGuideNodePos2: Vec3 = null;
    @property(Node)
    uiGuideNode3: Node = null;
    uiGuideNodePos3: Vec3 = null;

    @property(Node)
    PopPage: Node = null;
    @property(Node)
    home_diolog: Node = null;

    boardeffectTimes: number = 1;
    protected start(): void {
        // log(GlobalData.isCommer)
        if (GlobalData.isCommer) {
            this.runGuide();
        }
    }

    runGuide() {
        if (!GlobalData.isCommer) {
            this.node.active = false;
            return;
        }
        TouchMgr.ins.resumePromptBlock();
        // Tween.stopAll();
        Tween.stopAllByTarget(this.toucher.node);
        Tween.stopAllByTarget(this.block);
        // tween(this.toucher.node).stop()
        // tween(this.block).stop()

        tween(this.home_diolog)
            .to(0.5, { position: v3(300, 15, 0) })
            .to(0.5, { position: v3(250, 15, 0) })
            .union()
            .repeatForever()
            .start()
        this.guideTarPos = TouchMgr.ins.getNodeAToNodeBPoint(this.guidenode, this.node);
        this.selecOriPos = TouchMgr.ins.getNodeAToNodeBPoint(this.selecnode, this.node);
        this.uiGuideNodePos1 = TouchMgr.ins.getNodeAToNodeBPoint(this.uiGuideNode1, this.node);

        // console.log(LocalStorageMgr.getItem(LocalStorageMgr.isCommer_key, true));
        // console.log(LocalStorageMgr.getItem(LocalStorageMgr.guideRecord_key, 1));

        if (GlobalData.guideRecord == 1) {
            GridBlockMgr.ins.generateBoard(GridBoardData.customizeBoardData1);
            GridBlockMgr.ins.generareGuideSelectionBlock([BlocksAll[45], BlocksAll[45], BlocksAll[45]]);
            this.guide1();
        } else if (GlobalData.guideRecord == 2) {
            GridBlockMgr.ins.generateBoard(GridBoardData.customizeBoardData2);
            GridBlockMgr.ins.generareGuideSelectionBlock([BlocksAll[7], BlocksAll[7], BlocksAll[7]]);
            this.guide2();
        } else if (GlobalData.guideRecord == 3) {
            GridBlockMgr.ins.generateBoard(GridBoardData.customizeBoardData3);
            GridBlockMgr.ins.generareGuideSelectionBlock([BlocksAll[11], BlocksAll[11], BlocksAll[11]]);
            this.guide3();
        } else if (GlobalData.guideRecord == 4) {
            this.guide4();
        } else if (GlobalData.guideRecord == 5) {
            this.guide5();
        } else if (GlobalData.guideRecord == 6) {
            this.guide6();
        } else if (GlobalData.guideRecord == 7) {
            this.guide7()
        } else if (GlobalData.guideRecord == 8) {
            this.guide8();
        } else if (GlobalData.guideRecord == 9) {
            this.guide9();
        } else if (GlobalData.guideRecord == 10) {
            this.guide10();
        }

    }

    guide1() {
        this.home_diolog.active = false;
        this.toucher.node.active = true
        this.diolog.active = true;
        this.diolog.setPosition(v3(0, -500, 0));
        // this.diologLabel.string = '<color=#855933><b>拖动方块，填满即可消除</color>';


        let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("guide_Rtext_1");


        this.block.active = true;
        this.block.setPosition(this.selecOriPos);
        this.toucher.node.setPosition(this.selecOriPos.add(v3(100, -60, 0)));
        this.block.getComponent(BlockController).updateBlockSet(false, BlocksAll[45], 150);
        this.block.getComponent(BlockController).addBlockOffset(2);
        this.pointer.node.setPosition(v3(0, 0, 0))
        tween(this.toucher.node)
            .delay(0.2)
            .by(1, { position: v3(0, this.guideTarPos.y - this.selecOriPos.y, 0) })
            .delay(0.2)
            .by(0.01, { position: v3(0, -(this.guideTarPos.y - this.selecOriPos.y), 0) })
            .union()
            .repeatForever()
            .start()
        tween(this.block)
            .delay(0.2)
            .by(1, { position: v3(0, this.guideTarPos.y - this.selecOriPos.y, 0) })
            .delay(0.2)
            .by(0.01, { position: v3(0, -(this.guideTarPos.y - this.selecOriPos.y), 0) })
            .union()
            .repeatForever()
            .start()
    }

    guide2() {
        this.home_diolog.active = false;
        this.toucher.node.active = true
        this.diolog.active = false;
        this.block.active = true;
        this.block.setPosition(this.selecOriPos);
        this.toucher.node.setPosition(this.selecOriPos.add(v3(100, -60, 0)));
        this.block.getComponent(BlockController).updateBlockSet(false, BlocksAll[7], 150);
        this.block.getComponent(BlockController).addBlockOffset(2);
        this.pointer.node.setPosition(v3(0, 0, 0))
        // this.guideTarPos.add3f(0, -55, 0);
        tween(this.toucher.node)
            .delay(0.2)
            .by(1, { position: v3(0, this.guideTarPos.y - this.selecOriPos.y - 55, 0) })
            .delay(0.2)
            .by(0.01, { position: v3(0, -(this.guideTarPos.y - this.selecOriPos.y) + 55, 0) })
            .union()
            .repeatForever()
            .start()
        tween(this.block)
            .delay(0.2)
            .by(1, { position: v3(0, this.guideTarPos.y - this.selecOriPos.y - 55, 0) })
            .delay(0.2)
            .by(0.01, { position: v3(0, -(this.guideTarPos.y - this.selecOriPos.y) + 55, 0) })
            .union()
            .repeatForever()
            .start()
    }

    guide3() {
        this.home_diolog.active = false;
        this.toucher.node.active = true
        this.diolog.active = false;
        this.block.active = true;
        this.block.setPosition(this.selecOriPos);
        this.toucher.node.setPosition(this.selecOriPos.add(v3(100, -60, 0)));
        this.block.getComponent(BlockController).updateBlockSet(false, BlocksAll[11], 150);
        this.block.getComponent(BlockController).addBlockOffset(2);
        this.pointer.node.setPosition(v3(0, 0, 0))
        tween(this.toucher.node)
            .delay(0.2)
            .by(1, { position: v3(0, this.guideTarPos.y - this.selecOriPos.y - 55, 0) })
            .delay(0.2)
            .by(0.01, { position: v3(0, -(this.guideTarPos.y - this.selecOriPos.y) + 55, 0) })
            .union()
            .repeatForever()
            .start()
        tween(this.block)
            .delay(0.2)
            .by(1, { position: v3(0, this.guideTarPos.y - this.selecOriPos.y - 55, 0) })
            .delay(0.2)
            .by(0.01, { position: v3(0, -(this.guideTarPos.y - this.selecOriPos.y) + 55, 0) })
            .union()
            .repeatForever()
            .start()
    }

    guide6() {
        this.toucher.node.active = true
        tween(this.toucher.node)
            .to(0.2, { scale: v3(1.2, 1.2, 1) })
            .to(0.2, { scale: v3(1, 1, 1) })
            .union()
            .repeatForever()
            .start();
        this.block.active = false;
        this.mask.node.setPosition(this.uiGuideNodePos1)
        this.toucher.node.setPosition(this.uiGuideNodePos1.add(v3(220, -50, 0)));
        this.diolog.active = true;
        this.diolog.setPosition(this.toucher.node.getPosition().add(v3(0, -200, 0)));
        // this.diologLabel.string = '<color=#855933><b>可以提现了，快点点击看看吧</color>';
        let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("guide_Rtext_2");
        this.mask.node.active = true;
        this.mask.node.getComponent(UITransform).setContentSize(new Size(530, 140));
        this.shade.setPosition(v3(-this.mask.node.position.x, -this.mask.node.position.y, this.mask.node.position.z));
        this.shade.getComponent(UIOpacity).opacity = 150;
        // console.log('this.shade.getComponent(UIOpacity).opacity', this.shade.getComponent(UIOpacity).opacity);

        // tween(this.shade.getComponent(UIOpacity))
        //     .to(1, { opacity: 0 })
        //     .start();

    }

    async guide7() {
        tween(this.toucher.node).stop()
        tween(this.block).stop()
        let wdpage = this.PopPage.getChildByName('WithdrawPage')
        if (!wdpage || !wdpage.active) {
            let config =  await ServiceAPI.WithdrawConfig(GlobalData.userId);
            await Popup.Withdrawpage(config);
            this.uiGuideNode2 = this.PopPage.getChildByName('WithdrawPage').getComponent(WithdrawPage).uiGuideNode2;
            this.scheduleOnce(() => {
                this.toucher.node.active = true
                tween(this.toucher.node)
                    .to(0.2, { scale: v3(1.2, 1.2, 1) })
                    .to(0.2, { scale: v3(1, 1, 1) })
                    .union()
                    .repeatForever()
                    .start();
                this.uiGuideNodePos2 = TouchMgr.ins.getNodeAToNodeBPoint(this.uiGuideNode2, this.node);
                this.block.active = false;
                this.mask.node.setPosition(this.uiGuideNodePos2)
                this.toucher.node.setPosition(this.uiGuideNodePos2.add(v3(100, -100, 0)));
                this.diolog.active = true;
                this.diolog.setPosition(this.toucher.node.getPosition().add(v3(0, -200, 0)));
                // this.diologLabel.string = '<color=#855933><b>选择提现金额</color>';
                let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
                localizedLabel.setTextKeyAndOption("guide_Rtext_3");
                this.mask.node.active = true;
                this.mask.node.getComponent(UITransform).setContentSize(new Size(420, 205));
                this.shade.setPosition(v3(-this.mask.node.position.x, -this.mask.node.position.y, this.mask.node.position.z));
                this.shade.getComponent(UIOpacity).opacity = 150;
            }, 0.15)
        } else {
            this.uiGuideNode2 = this.PopPage.getChildByName('WithdrawPage').getComponent(WithdrawPage).uiGuideNode2;
            this.scheduleOnce(() => {
                this.toucher.node.active = true
                tween(this.toucher.node)
                    .to(0.2, { scale: v3(1.2, 1.2, 1) })
                    .to(0.2, { scale: v3(1, 1, 1) })
                    .union()
                    .repeatForever()
                    .start();
                this.uiGuideNodePos2 = TouchMgr.ins.getNodeAToNodeBPoint(this.uiGuideNode2, this.node);
                this.block.active = false;
                this.mask.node.setPosition(this.uiGuideNodePos2)
                this.toucher.node.setPosition(this.uiGuideNodePos2.add(v3(100, -100, 0)));
                this.diolog.active = true;
                this.diolog.setPosition(this.toucher.node.getPosition().add(v3(0, -200, 0)));
                // this.diologLabel.string = '<color=#855933><b>选择提现金额</color>';
                let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
                localizedLabel.setTextKeyAndOption("guide_Rtext_3");
                this.mask.node.active = true;
                this.mask.node.getComponent(UITransform).setContentSize(new Size(420, 205));
                this.shade.setPosition(v3(-this.mask.node.position.x, -this.mask.node.position.y, this.mask.node.position.z));
                this.shade.getComponent(UIOpacity).opacity = 150;
            }, 0.15)
        }



    }

    async guide8() {
        tween(this.toucher.node).stop()
        tween(this.block).stop()
        let wdpage = this.PopPage.getChildByName('WithdrawPage')
        if (!wdpage || !wdpage.active) {
            // let config = await ServiceAPI.WithdrawConfig(GlobalData.userId);
            // await Popup.Withdrawpage(config);
            this.uiGuideNode3 = this.PopPage.getChildByName('WithdrawPage').getComponent(WithdrawPage).uiGuideNode3
            this.scheduleOnce(() => {
                tween(this.toucher.node)
                    .to(0.2, { scale: v3(1.2, 1.2, 1) })
                    .to(0.2, { scale: v3(1, 1, 1) })
                    .union()
                    .repeatForever()
                    .start();
                this.uiGuideNodePos3 = TouchMgr.ins.getNodeAToNodeBPoint(this.uiGuideNode3, this.node);
                this.block.active = false;
                this.mask.node.setPosition(this.uiGuideNodePos3)
                this.toucher.node.setPosition(this.uiGuideNodePos3.add(v3(0, -50, 0)));
                this.diolog.active = true;
                this.diolog.setPosition(this.toucher.node.getPosition().add(v3(0, -200, 0)));
                // this.diologLabel.string = '<color=#855933><b>点击提现</color>';
                let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
                localizedLabel.setTextKeyAndOption("guide_Rtext_4");
                this.mask.node.active = true;
                this.mask.node.getComponent(UITransform).setContentSize(new Size(600, 170));
                this.shade.setPosition(v3(-this.mask.node.position.x, -this.mask.node.position.y, this.mask.node.position.z));
                this.shade.getComponent(UIOpacity).opacity = 150;
            }, 0.15)
        } else {
            this.uiGuideNode3 = this.PopPage.getChildByName('WithdrawPage').getComponent(WithdrawPage).uiGuideNode3
            this.scheduleOnce(() => {
                tween(this.toucher.node)
                    .to(0.2, { scale: v3(1.2, 1.2, 1) })
                    .to(0.2, { scale: v3(1, 1, 1) })
                    .union()
                    .repeatForever()
                    .start();
                this.uiGuideNodePos3 = TouchMgr.ins.getNodeAToNodeBPoint(this.uiGuideNode3, this.node);
                this.block.active = false;
                this.mask.node.setPosition(this.uiGuideNodePos3)
                this.toucher.node.setPosition(this.uiGuideNodePos3.add(v3(0, -50, 0)));
                this.diolog.active = true;
                this.diolog.setPosition(this.toucher.node.getPosition().add(v3(0, -200, 0)));
                // this.diologLabel.string = '<color=#855933><b>点击提现</color>';
                let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
                localizedLabel.setTextKeyAndOption("guide_Rtext_4");
                this.mask.node.active = true;
                this.mask.node.getComponent(UITransform).setContentSize(new Size(600, 170));
                this.shade.setPosition(v3(-this.mask.node.position.x, -this.mask.node.position.y, this.mask.node.position.z));
                this.shade.getComponent(UIOpacity).opacity = 150;
            }, 0.15)
        }
    }
    async guide4() {
        this.block.active = false;
        this.toucher.node.active = false;
        this.diolog.active = false;
        this.mask.node.active = false;

        this.completeCurGuide(5);
    }

    async guide9() {
        this.home_diolog.active = true;
        tween(this.home_diolog)
            .to(0.5, { position: v3(300, 15, 0) })
            .to(0.5, { position: v3(250, 15, 0) })
            .union()
            .repeatForever()
            .start()

        GridBlockMgr.ins.generateBoard_effect(GridBoardData.customizeBoardData4);
        this.scheduleOnce(() => {
            GridBlockMgr.ins.generateSelectionBlock(false, [BlocksAll[15], BlocksAll[2], BlocksAll[15]])
        }, 2);
        this.node.active = true;
        this.block.active = false;
        this.toucher.node.active = false

        // this.diologLabel.string = `<color=#855933><b>再消除</color><color=#FF0000><b>1次</color><color=#855933><b>方块可获得新人福利,最高</color><color=#FF0000><b>${MainGameLogic.ins.formatCashValue(this.newComerReward.showAmount)}</color>`;
        // console.log(this.newComerReward);

        // if (this.newComerReward) {
        this.diolog.active = true;
        this.diolog.setPosition(v3(0, 0, 0));
        // let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
        // localizedLabel.setTextKeyAndOption("guide_Rtext_5", 1, MainGameLogic.ins.formatCashValue(this.newComerReward.showAmount));
        // let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
        // localizedLabel.setTextKeyAndOption("guide_Rtext_7", MainGameLogic.ins.formatCashValue(this.newComerReward.showAmount - this.newComerReward.cashTotal), MainGameLogic.ins.formatCashValue(this.newComerReward.showAmount));
        // }
        this.mask.node.active = true;
        this.mask.node.getComponent(UITransform).setContentSize(new Size(0, 0));
        this.mask.node.setPosition(v3(0, 0, 0))
        this.shade.setPosition(v3(0, 0, 0));
        this.diolog.getComponent(UIOpacity).opacity = 0;
        tween(this.diolog.getComponent(UIOpacity))
            .to(0.5, { opacity: 255 })
            .delay(2)
            .to(0.5, { opacity: 0 })
            .call(() => {
                this.node.active = false;
            })
            .start()

        let config = GlobalData.getDataStore().WithdrawConfig// await ServiceAPI.WithdrawConfig(GlobalData.userId);
        let tarCashAmount = config.cashWithdrawConfig[0].cashAmount;

        let needCash = tarCashAmount - GlobalData.curCashTotal
        if (needCash <= 0) {
            tarCashAmount = config.cashWithdrawConfig[1].cashAmount;
        }
        let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("guide_Rtext_7", MainGameLogic.ins.formatCashValue(tarCashAmount - GlobalData.curCashTotal), MainGameLogic.ins.formatCashValue(tarCashAmount));



    }

    async guide5() {
        // this.pop.Newcomerpage(this.pageConfig);
        this.newComerReward = await ServiceAPI.GetNewComerReward(GlobalData.userId);
        MainGameLogic.ins.sssspaciCashTotal = this.newComerReward.cashAmount
        await Popup.Newcomerpage(this.newComerReward);
    }

    guide10() {
        return
        this.node.active = true;
        this.block.active = false;
        this.toucher.node.active = false
        this.diolog.active = true;
        this.diolog.setPosition(v3(0, 0, 0));
        // this.diologLabel.string = '<color=#855933><b>再赚</color><color=#FF0000><b>112元</color><color=#855933><b>，即可提现</color><color=#FF0000><b>500元</color><color=#855933><b>\n继续玩游戏赚钱吧！</color>';
        let localizedLabel = this.diologLabel.node.getComponent(LocalizedLabel);
        localizedLabel.setTextKeyAndOption("guide_Rtext_7", MainGameLogic.ins.formatCashValue(this.newComerReward.showAmount - this.newComerReward.cashTotal), MainGameLogic.ins.formatCashValue(this.newComerReward.showAmount));
        this.mask.node.active = true;
        this.mask.node.getComponent(UITransform).setContentSize(new Size(0, 0));
        this.mask.node.setPosition(v3(0, 0, 0))
        this.shade.setPosition(v3(0, 0, 0));

        this.diolog.getComponent(UIOpacity).opacity = 0;
        tween(this.diolog.getComponent(UIOpacity))
            .to(0.5, { opacity: 255 })
            .delay(2)
            .to(0.5, { opacity: 0 })
            .call(() => {
                this.node.active = false;
            })
            .start()
    }

    completeCurGuide(record) {
        GlobalData.guideRecord = record;
        LocalStorageMgr.setItem(LocalStorageMgr.guideRecord_key, GlobalData.guideRecord);

        this.runGuide();
        if (GlobalData.guideRecord >= 9) {
            GlobalData.isCommer = false;
            LocalStorageMgr.setItem(LocalStorageMgr.isCommer_key, GlobalData.isCommer);
        }
    }

}