import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('BlockBa')
export class BlockBa extends Component {


    public id: any;
    public state: number = 0;//0不显示，1显示
    start() {

    }

    update(deltaTime: number) {
        
    }

    init(se, num) {
        this.id = num;
        this.state = 0;
        
    }
    changeState(wan,ma) {
        if(wan==0){
            if(ma){
                //鼠标没抬起方块可以放置的临时状态
                
                this.state = 3;
            }else{
                if(this.state == 3){
                     //临时状态改成初始状态
                    this.state = 0;
                    
                }
            }
        }else{
            if(ma){
                //改成初始状态
                this.state = 0;
            }else{
                //鼠标抬起 方块放下 状态转换为被占状态
                this.state = 1;
            }
        }
    }
}

