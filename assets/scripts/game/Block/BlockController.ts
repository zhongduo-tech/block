import { _decorator, Color, Component, Node, Sprite, UIOpacity, Vec2, Vec3 } from 'cc';
import { BlockConfig, BlockOffset, BlocksAll } from './BlockData';
import { GridBlockMgr } from '../GridBlockMgr';
const { ccclass, property } = _decorator;

@ccclass('BlockController')
export class BlockController extends Component {

    @property(Node)
    blockSet: Node = null; // 25个子节点的父节点

    public curBlockConfig: BlockConfig = null;


    /**
     * 根据二维数组配置显示/隐藏子节点
     * @param blockConfig 数据
     * @param opacity 透明度
     * @returns 
     */
    updateBlockSet(detect: boolean, blockConfig?: BlockConfig, opacity?: number) {
        if (blockConfig) {
            this.curBlockConfig = blockConfig;
        }
        const curbBlockArr = this.curBlockConfig.block_arr
        const children = this.blockSet.children;

        // 确保有25个子节点
        if (children.length !== 25) {
            console.error('blockSet should have exactly 25 children');
            return;
        }

        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                const index = i * 5 + j;
                const node = children[index];
                if (curbBlockArr[i][j] === 1) {
                    node.active = true;  // 显示节点
                    if (opacity) {
                        node.getComponent(UIOpacity).opacity = opacity;
                        node.getComponent(Sprite).color = new Color().fromHEX('#B9B9B9');
                    } else {
                        node.getComponent(UIOpacity).opacity = 255;
                    }

                } else {
                    node.active = false; // 隐藏节点
                    // node.getComponent(UIOpacity).opacity = 100;
                }
            }
        }
        if (detect) {
            GridBlockMgr.ins.detectIsCanPlaced(this.curBlockConfig.block_arr, this.node)
        }



    }

    checkGray() {
        GridBlockMgr.ins.detectIsCanPlaced(this.curBlockConfig.block_arr, this.node)
    }

    /**
     * 根据数据调整偏移
     * @param scaleZoom 缩放比例
     */
    addBlockOffset(scaleZoom: number) {
        if (!this.curBlockConfig) {
            // console.error("curBlockConfig is null or undefined");
            return;
        }
        const blockOffset = this.curBlockConfig.block_offset;
        // console.log("Block offset:", blockOffset);
        // console.log("Original Position:", this.node.position);

        let offset = new Vec3();
        if (blockOffset === BlockOffset.UP) {
            offset = new Vec3(0, 122.5 / scaleZoom, 0);
        } else if (blockOffset === BlockOffset.DOWN) {
            offset = new Vec3(0, -122.5 / scaleZoom, 0);
        } else if (blockOffset === BlockOffset.LEFT) {
            offset = new Vec3(-122.5 / scaleZoom, 0, 0);
        } else if (blockOffset === BlockOffset.RIGHT) {
            offset = new Vec3(122.5 / scaleZoom, 0, 0);
        } else if (blockOffset === BlockOffset.LEFTANDDOWN) {
            offset = new Vec3(-122.5 / scaleZoom, -122.5 / scaleZoom, 0);
        } else if (blockOffset === BlockOffset.LEFTANDUP) {
            offset = new Vec3(-122.5 / scaleZoom, 122.5 / scaleZoom, 0);
        } else if (blockOffset === BlockOffset.RIGHTANDDOWN) {
            offset = new Vec3(122.5 / scaleZoom, -122.5 / scaleZoom, 0);
        } else if (blockOffset === BlockOffset.RIGHTANDUP) {
            offset = new Vec3(122.5 / scaleZoom, 122.5 / scaleZoom, 0);
        }

        // console.log("Offset to be added:", offset);
        this.node.position = this.node.position.add(offset);
        // console.log("New Position:", this.node.position);
    }

    start() {
        // 示例图块配置
        // const blockConfig = [
        //     [0, 0, 0, 0, 0],
        //     [0, 0, 1, 0, 0],
        //     [0, 0, 1, 1, 0],
        //     [0, 0, 0, 0, 0],
        //     [0, 0, 0, 0, 0]
        // ];

        // 更新 blockSet
        // this.updateBlockSet(blockConfig);
    }


}


