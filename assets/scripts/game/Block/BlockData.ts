
//Block的Type
export enum BlockType {
    TYPE_A,
    TYPE_B,
    TYPE_C,
    TYPE_D,
    TYPE_E,
    TYPE_F,
    TYPE_G,
    TYPE_H,
    TYPE_I,
    TYPE_J,
    TYPE_K,
    TYPE_L,
    TYPE_M,
    TYPE_N,
}
//Block偏移方向
export enum BlockOffset {
    NONE,
    UP,
    DOWN,
    LEFT,
    RIGHT,
    LEFTANDUP,
    LEFTANDDOWN,
    RIGHTANDUP,
    RIGHTANDDOWN
}

export enum BlockState {
    HIDE,
    SHOW
}
export interface Region {
    i: number;
    j: number;
}

export interface ReConfig {
    iscanplaced: boolean;
    indexarr: number[];
}

export interface BlockBaData {
    index_i: number,
    index_j: number,
    block_ba_node: any,
    block_state: BlockState
}
// 定义块的数据结构
export class BlockConfig {
    block_arr: number[][];
    block_type: BlockType;
    block_type_index: number;
    block_offset: BlockOffset;

    constructor(arr: number[][], type: BlockType, index: number, offset: BlockOffset) {
        this.block_arr = arr;
        this.block_type = type;
        this.block_type_index = index;
        this.block_offset = offset
    }
}


// 存储多个块的静态数据
export const BlocksAll: BlockConfig[] = [
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_A, 0, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_B, 0, BlockOffset.DOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_B, 1, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_B, 2, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_B, 3, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_C, 0, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_C, 1, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_D, 0, BlockOffset.LEFTANDUP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_D, 1, BlockOffset.RIGHTANDUP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_D, 2, BlockOffset.RIGHTANDDOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_D, 3, BlockOffset.LEFTANDDOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_E, 0, BlockOffset.LEFTANDDOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_E, 1, BlockOffset.LEFTANDUP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_E, 2, BlockOffset.RIGHTANDUP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_E, 3, BlockOffset.RIGHTANDDOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],//4个格子的
        [0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_F, 0, BlockOffset.LEFTANDDOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],//4个格子的
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_F, 1, BlockOffset.LEFTANDUP),
    new BlockConfig([
        [0, 0, 0, 0, 0],//4个格子的
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_F, 2, BlockOffset.RIGHTANDUP),
    new BlockConfig([
        [0, 0, 0, 0, 0],//4个格子的
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_F, 3, BlockOffset.RIGHTANDDOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_G, 0, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_G, 1, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_G, 2, BlockOffset.DOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_G, 3, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_H, 0, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_H, 1, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_H, 2, BlockOffset.DOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_H, 3, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_I, 0, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_I, 1, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_I, 2, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_I, 3, BlockOffset.DOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_J, 0, BlockOffset.DOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_J, 1, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_J, 2, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_J, 3, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],//5个的
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_K, 0, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],//5个的
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_K, 1, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_L, 0, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_L, 1, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_L, 2, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_L, 3, BlockOffset.NONE),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_M, 0, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_M, 1, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_M, 2, BlockOffset.DOWN),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_M, 3, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_N, 0, BlockOffset.LEFT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_N, 1, BlockOffset.UP),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_N, 2, BlockOffset.RIGHT),
    new BlockConfig([
        [0, 0, 0, 0, 0],
        [0, 0, 1, 1, 0],
        [0, 1, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0]
    ], BlockType.TYPE_N, 3, BlockOffset.DOWN),
];

// 权重数组
export const BlockWeights = {
    [BlockType.TYPE_A]: 0.07,
    [BlockType.TYPE_B]: 0.07,
    [BlockType.TYPE_C]: 0.035,
    [BlockType.TYPE_D]: 0.17,
    [BlockType.TYPE_E]: 0.085,
    [BlockType.TYPE_F]: 0.085,
    [BlockType.TYPE_G]: 0.07,
    [BlockType.TYPE_H]: 0.07,
    [BlockType.TYPE_I]: 0.07,
    [BlockType.TYPE_J]: 0.07,
    [BlockType.TYPE_K]: 0.035,
    [BlockType.TYPE_L]: 0.07,
    [BlockType.TYPE_M]: 0.07,
    [BlockType.TYPE_N]: 0.07,
};


export var GridBoardData = {

    customizeBoardData1: [
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0]
    ],
    customizeBoardData2: [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 1, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ],
    customizeBoardData3: [
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 0, 0, 0]
    ],
    customizeBoardData4: [
        [1, 1, 0, 0, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 1, 1, 1],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [1, 1, 0, 0, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 1, 1]
    ],
    customizeBoardData5: [
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0]
    ]


}

