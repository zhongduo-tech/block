import { assetManager, AudioClip, AudioSource, director, Node, sys } from 'cc';

export class AudioManager {

    /** 单例模式 */
    private static _ins: AudioManager;

    constructor() {
        let audioMgr = new Node();
        audioMgr.name = '__audioMgr__';

        director.getScene().addChild(audioMgr);
        director.addPersistRootNode(audioMgr);

        this._audioSource = audioMgr.addComponent(AudioSource);
        AudioManager._ins = this;

        // 从本地存储恢复静音状态
        const muteState = sys.localStorage.getItem('audio_mute');
        if (muteState !== null) {
            this._isMuted = muteState === 'true';
            this._updateVolume();
        }
    }

    public static get ins(): AudioManager {
        if (!AudioManager._ins) {
            AudioManager._ins = new AudioManager();
        }
        return AudioManager._ins;
    }

    private _audioSource: AudioSource;
    private _musicVolume = 1.0;
    private _musicVolumeScale = 1.0;
    private _soundVolume = 1.0;
    private _isMuted = false;

    public get audioSource() {
        return this._audioSource;
    }

    public set musicVolume(v: number) {
        this._musicVolume = v;
        this._updateVolume();
    }

    public set soundVolume(v: number) {
        this._soundVolume = v;
    }

    public get mute() {
        return this._isMuted;
    }

    public set mute(muted: boolean) {
        this._isMuted = muted;
        sys.localStorage.setItem('audio_mute', muted.toString());  // 存储静音状态
        this._updateVolume();
    }

    private _updateVolume() {
        if (this._isMuted) {
            this._audioSource.volume = 0;
        } else {
            this._audioSource.volume = this._musicVolume * this._musicVolumeScale;
        }
    }

    /**
     * 播放短音频, 比如打击音效，爆炸音效等
     */
    playOneShot(sound: AudioClip | string, volume: number = 1.0, bundleName: string = 'resources') {
        if (sound instanceof AudioClip) {
            this._audioSource.playOneShot(sound, volume * this._soundVolume);
        } else {
            let bundle = assetManager.getBundle(bundleName);
            bundle.load(sound, (err, clip: AudioClip) => {
                if (err) {
                    console.log(err);
                } else {
                    this._audioSource.playOneShot(clip, volume * this._soundVolume);
                }
            });
        }
    }

    /**
     * 播放长音频，比如背景音乐，支持循环播放
     */
    play(sound: AudioClip | string, volume: number = 1.0, loop: boolean = true, bundleName: string = 'resources') {
        this._musicVolumeScale = volume;
        if (sound instanceof AudioClip) {
            this._audioSource.stop();
            this._audioSource.clip = sound;
            this._audioSource.loop = loop;  // 设置是否循环播放
            this._audioSource.play();
            this._updateVolume();
        } else {
            let bundle = assetManager.getBundle(bundleName);
            bundle.load(sound, (err, clip: AudioClip) => {
                if (err) {
                    console.log(err);
                } else {
                    this._audioSource.stop();
                    this._audioSource.clip = clip;
                    this._audioSource.loop = loop;  // 设置是否循环播放
                    this._audioSource.play();
                    this._updateVolume();
                }
            });
        }
    }

    /**
     * 停止音频播放
     */
    stop() {
        this._audioSource.stop();
    }

    /**
     * 暂停音频播放
     */
    pause() {
        this._audioSource.pause();
    }

    /**
     * 恢复音频播放
     */
    resume() {
        this._audioSource.play();
        this._updateVolume();
    }
}
